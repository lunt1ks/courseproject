import {EntityRepository, Repository} from "typeorm";
import {Initiative} from "./initiative.entity";

@EntityRepository(Initiative)
export class InitiativeRepository extends Repository<Initiative> {

}