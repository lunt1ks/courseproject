import {Module} from '@nestjs/common';
import {InitiativesController} from './initiatives.controller';
import {InitiativesService} from './initiatives.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {InitiativeRepository} from "./initiative.repository";
import {AuthModule} from "../auth/auth.module";

@Module({
    imports: [TypeOrmModule.forFeature([InitiativeRepository]),
        AuthModule],
    controllers: [InitiativesController],
    providers: [InitiativesService]
})
export class InitiativesModule {
}
