import {
    ConflictException,
    Injectable,
    NotFoundException,
    InternalServerErrorException,
    ForbiddenException
} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {InitiativeRepository} from "./initiative.repository";
import {User} from "../auth/entities/user.entity";
import {Initiative} from "./initiative.entity";
import {Objective} from "../objectives/entities/objective.entity";
import {UpdateInitiativeDto} from "./dto/update-initiative.dto";
import {CreateInitiativeDto} from "./dto/create-initiative.dto";
import {Company} from "../companies/company.entity";

@Injectable()
export class InitiativesService {
    constructor(@InjectRepository(InitiativeRepository)
                private initiativeRepository: InitiativeRepository) {
    }

    async createInitiative(user: User,
                           createInitiativeDto: CreateInitiativeDto,
                           companyId: number,
                           objectiveId: number): Promise<Initiative> {
        const company = await Company.findOne(companyId, {relations: ['objectives', "objectives.initiatives", 'objectives.users']});
        if (!company) throw new NotFoundException('No such company with id ' + companyId)


        const objective: Objective = company.objectives.find(item => item.id === objectiveId);
        if (!objective) {
            throw new NotFoundException('No such objective with id ' + objectiveId)
        }
        if (!await this.permissions(company.ownerId, user, objective)) {
            throw new ForbiddenException('Not enough permissions');
        }

        const initiative: Initiative = new Initiative();
        /*eslint-disable-next-line*/
        initiative.owner_id = user.id;
        Object.assign(initiative, createInitiativeDto);

        try {
            await initiative.save()
        } catch (ex) {
            if (ex.code === '23505') throw new ConflictException('Initiative with that name is already exists');
            else {
                throw new InternalServerErrorException(`Error creating initiative: ${ex.message}`)
            }
        }

        objective.initiatives.push(initiative)
        await objective.save()
        return initiative
    }

    async getInitiatives(): Promise<Initiative[]> {
        return await this.initiativeRepository.find();
    }


    async getInitiativeById(id: number, user: User): Promise<Initiative> {
        const initiative = await this.initiativeRepository.findOne(id, {relations: ['actions', "actions.user", "objective", "objective.users", "objective.company"]});
        if (!initiative.objective.users.some(item => item.id === user.id)) {
            if(initiative.objective.company.ownerId!==user.id) {
                if (user.role !== 'superAdmin')  {
                    throw new ForbiddenException("You are not in objective")
                }
            }
        }
        return initiative;
    }

    async updateOwner(user: User, userId: number, id: number) {
        const initiative = await this.initiativeRepository.findOne(id,
            {relations: ["objective", "objective.users", "objective.company"]});
        if (!initiative.objective.users.some(user => user.id === userId)) {
            throw new NotFoundException('User is not assigned to objective');
        }

        if (user.role !== 'superAdmin') {
            if (user.id !== initiative.objective.company.ownerId) {
                throw new ForbiddenException('You dont have permissions to update initiative');
            }
        }
        initiative.owner_id = userId;
        return initiative.save();
    }

    async updateInitiative(user: User, id: number, updateInitiativeDto: UpdateInitiativeDto): Promise<Initiative> {
        const initiative = await this.initiativeRepository.findOne(id,
            {relations: ["objective", "objective.users", "objective.company"]});
        if (!initiative) throw new NotFoundException("No initiative with id" + id);
        if (!await this.permissions(initiative.objective.company.ownerId, user, initiative.objective)) {
            throw new ForbiddenException('Not enough permissions');
        }

        if (!updateInitiativeDto.name) {
            updateInitiativeDto.name = initiative.name;
        }

        if (!updateInitiativeDto.end_date) {
            /*eslint-disable-next-line*/
            updateInitiativeDto.end_date = initiative.end_date;
        }

        if (!updateInitiativeDto.notes) {
            updateInitiativeDto.notes = initiative.notes;
        }

        Object.assign(initiative, updateInitiativeDto)
        return await initiative.save()
    }

    async deleteInitiative(user: User, id: number): Promise<void> {
        const initiative = await this.initiativeRepository.findOne(id,
            {relations: ['objective', 'objective.company']})
        if (!initiative) throw new NotFoundException('No initiative with such id');
        if (user.role !== 'superAdmin') {
            if (initiative.objective.company.ownerId !== user.id) {
                throw new ForbiddenException("You dont have permissions to delete initiative with id" + id)
            }
        }
        try {
            const result = await this.initiativeRepository.delete({id});
            if (result.affected === 0) {
                throw new NotFoundException(`Task with ID "${id}" not found`)
            }
        } catch
            (ex) {
            throw new InternalServerErrorException(`${ex.message}`);
        }
    }

    private permissions = async (ownerId: number, user: User, objective: Objective) => {
        if (objective.users.some(user => user.id)) return true
        if (user.role !== 'superAdmin') {
            if (ownerId !== user.id) {
                if (user.role !== 'collaborator') {
                    return false;
                }
            }
        }
        return true;
    };

}
