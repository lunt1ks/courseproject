import {IsDate, IsNotEmpty, IsOptional, IsString, MinLength} from "class-validator";

export class CreateInitiativeDto {

    @IsNotEmpty()
    name:string

    @IsNotEmpty()
    end_date

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    notes: string;
}