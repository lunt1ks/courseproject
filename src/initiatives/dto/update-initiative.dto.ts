import { IsOptional, IsString, IsNotEmpty } from "class-validator";

export class UpdateInitiativeDto{

    @IsOptional()
    @IsString()
    @IsNotEmpty()
    name: string;

    @IsOptional()
    @IsString()
    @IsNotEmpty()
    end_date

    @IsOptional()
    @IsString()
    @IsNotEmpty()
    notes: string;
}