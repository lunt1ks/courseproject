import {BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, Unique} from "typeorm";
import {Objective} from "../objectives/entities/objective.entity";
import {Action} from "../actions/action.entity";

@Entity()
export class Initiative extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string;

    @Column({type:"date"})
    end_date

    @Column({nullable: true})
    notes: string;

    @Column()
    owner_id:number;

    @ManyToOne(type => Objective, objective=>objective.initiatives,
        {onUpdate:"CASCADE", onDelete:"CASCADE"})
    objective:Objective;

    @OneToMany(type=>Action, action=>action.initiative, {cascade:true})
    actions:Action[]

}
