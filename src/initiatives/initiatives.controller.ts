import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    UseGuards,
    ValidationPipe
} from '@nestjs/common';
import {InitiativesService} from "./initiatives.service";
import {CreateInitiativeDto} from "./dto/create-initiative.dto";
import {GetUser} from "../auth/custom-decorators/get-user.decorator";
import {User} from "../auth/entities/user.entity";
import {Initiative} from "./initiative.entity";
import {UpdateInitiativeDto} from "./dto/update-initiative.dto";
import {AuthGuard} from "@nestjs/passport";
import {Roles} from "../auth/custom-decorators/roles.decorator";
import {RolesGuard} from 'src/auth/custom-guards/roles.guard';

@Controller('api/initiatives')
export class InitiativesController {
    constructor(private initiativesService: InitiativesService) {
    }

    @Roles('superAdmin', 'admin', 'collaborator', 'contributor')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Post('/:objectiveId/:companyId')
    async createInitiative(
        @Param('objectiveId', ParseIntPipe) objectiveId: number,
        @Param('companyId', ParseIntPipe) companyId: number,
        @Body(ValidationPipe)createInitiativeDto: CreateInitiativeDto,
        @GetUser() user: User): Promise<Initiative> {
        return await this.initiativesService.createInitiative(user, createInitiativeDto, companyId, objectiveId);
    }

    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Get()
    async getInitiatives(): Promise<Initiative[]> {
        return await this.initiativesService.getInitiatives();
    }

    @UseGuards(AuthGuard())
    @Get('/:id')
    async getInitiativeById(@Param('id', ParseIntPipe) id: number,
                            @GetUser()user: User): Promise<Initiative> {
        return this.initiativesService.getInitiativeById(id, user);
    }

    @Roles('superAdmin', 'admin', 'collaborator', 'contributor')
    @UseGuards(AuthGuard(), RolesGuard)
    @Patch('/:id')
    async updateInitiative(
        @GetUser()user: User,
        @Param('id', ParseIntPipe) id: number,
        @Body(ValidationPipe)updateInitiativeDto: UpdateInitiativeDto) {
        return await this.initiativesService.updateInitiative(user, id, updateInitiativeDto)
    }

    @Roles('superAdmin', 'admin')
    @UseGuards(AuthGuard(), RolesGuard)
    @Patch('owner/:id')
    async updateInitiativeOwner(
        @GetUser()user: User,
        @Param('id', ParseIntPipe) id: number,
        @Body('userId', ParseIntPipe)userId: number) {
        return await this.initiativesService.updateOwner(user, userId, id)
    }

    @Roles('superAdmin', 'admin')
    @UseGuards(AuthGuard(), RolesGuard)
    @Delete('/:id')
    async deleteInitiative(
        @GetUser()user: User,
        @Param('id', ParseIntPipe) id: number) {
        return await this.initiativesService.deleteInitiative(user, id)
    }
}
