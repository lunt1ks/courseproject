import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ConfigModule} from './config/config.module';
import {ConfigService} from './config/config.service';
import {typeOrmConfig} from './config/typeorm.config';
import {AuthModule} from './auth/auth.module';
import {CompaniesModule} from './companies/companies.module';
import {ObjectivesModule} from './objectives/objectives.module';
import {KpiModule} from './kpi/kpi.module';
import {InitiativesModule} from "./initiatives/initiatives.module";
import {ActionsModule} from './actions/actions.module';
import {APP_GUARD, Reflector} from '@nestjs/core';
import {RolesGuard} from './auth/custom-guards/roles.guard';
import {InviteModule} from "./invite/invite.module";
import { ScheduleModule } from '@nestjs/schedule';
import { IndicatorsModule } from './indicators/indicators.module';


@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            imports: [
                ConfigModule
            ],
            useFactory: (configService: ConfigService) => {
                return typeOrmConfig(configService);
            },
            inject: [
                ConfigService
            ]
        }),
        AuthModule,
        IndicatorsModule,
        CompaniesModule,
        ObjectivesModule,
        KpiModule,
        InitiativesModule,
        ActionsModule,
        InviteModule,
        ScheduleModule.forRoot(),
        IndicatorsModule
    ],

    providers: [
        {
            provide: APP_GUARD,
            useValue: [RolesGuard, Reflector]
        },
    ],
})
export class AppModule {
}
