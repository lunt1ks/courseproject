import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    UseGuards,
    ValidationPipe
} from '@nestjs/common';
import {KpiService} from "./kpi.service";
import {UpdateKpiDto} from "./dto/update-kpi.dto";
import {CreateKpiDto} from "./dto/create-kpi.dto";
import {AuthGuard} from "@nestjs/passport";
import {GetUser} from "../auth/custom-decorators/get-user.decorator";
import {User} from "../auth/entities/user.entity";
import {Roles} from "../auth/custom-decorators/roles.decorator";
import {RolesGuard} from 'src/auth/custom-guards/roles.guard';
import {KpiLog} from "./entities/kpi-log.entity";

@Controller('api/kpi')
export class KpiController {
    constructor(private kpiService: KpiService) {
    }

    @Roles('superAdmin', 'admin')
    @UseGuards(AuthGuard('jwt'))
    @Post('/:objective_id')
    async createKpi(@Body(ValidationPipe) createKpiDto: CreateKpiDto,
                    @Param('objective_id', ParseIntPipe) objId: number,
                    @GetUser()user: User) {
        return await this.kpiService.createKpi(objId, createKpiDto, user);
    }

    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Get()
    async getAllKpi() {
        return await this.kpiService.getKpis();
    }

    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Get('objective/:objectiveId')
    async getKpisForObjective() {
        return await this.kpiService.getKpis();
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('logs/:kpiId')
    async getKpiLogs(@Param('kpiId', ParseIntPipe) kpiId: number): Promise<KpiLog[]> {
        return await this.kpiService.getKpiLogs(kpiId);
    }


    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Get('/:id')
    async getKpiById(@Param('id', ParseIntPipe) id: number) {
        return await this.kpiService.getKpiById(id);
    }

    @Roles('superAdmin', 'admin', 'collaborator')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Patch('/:id')
    async updateKpi(@Param('id', ParseIntPipe) id: number,
                    @Body(ValidationPipe) updateKpiDto: UpdateKpiDto,
                    @GetUser()user: User) {
        return await this.kpiService.updateKpi(id, updateKpiDto, user);
    }

    @Roles('superAdmin', 'admin','collaborator')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Delete(':id')
    async deleteKpi(@Param('id', ParseIntPipe) id: number,
                    @GetUser()user: User) {
        return await this.kpiService.deleteKpi(id, user)
    }
}
