import {EntityRepository, Repository} from "typeorm";
import {Kpi} from "./entities/kpi.entity";

@EntityRepository(Kpi)
export class KpiRepository extends Repository<Kpi> {

}