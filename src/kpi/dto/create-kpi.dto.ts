import { IsNotEmpty, IsString, IsNumber, IsOptional} from "class-validator";

export class CreateKpiDto {

    @IsString()
    @IsNotEmpty()
    name:string;

    @IsNumber()
    goal: number;

    @IsNumber()
    current_value: number;

    @IsOptional()
    @IsString()
    units: string;
}
