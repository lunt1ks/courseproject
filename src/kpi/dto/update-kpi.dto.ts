import {IsNotEmpty, IsNumber, IsOptional, IsString, MinLength} from "class-validator";

export class UpdateKpiDto{
    @IsOptional()
    @IsString()
    @IsNotEmpty()
    name: string;

    @IsOptional()
    @IsNumber()
    current_value: number;

    @IsOptional()
    @IsNumber()
    goal: number;

    @IsOptional()
    @IsString()
    units: string;

}