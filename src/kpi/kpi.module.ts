import {Module} from '@nestjs/common';
import {KpiController} from './kpi.controller';
import {KpiService} from './kpi.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {KpiRepository} from "./kpi.repository";

@Module({
    imports: [TypeOrmModule.forFeature([KpiRepository]),],
    controllers: [KpiController],
    providers: [KpiService]
})
export class KpiModule {
}
