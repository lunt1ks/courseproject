import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { KpiRepository } from "./kpi.repository";
import { InjectRepository } from "@nestjs/typeorm";
import { Kpi } from "./entities/kpi.entity";
import { CreateKpiDto } from "./dto/create-kpi.dto";
import { UpdateKpiDto } from "./dto/update-kpi.dto";
import { Objective } from "../objectives/entities/objective.entity";
import { User } from "../auth/entities/user.entity";
import { KpiLog } from "./entities/kpi-log.entity";


@Injectable()
export class KpiService {
    constructor(@InjectRepository(KpiRepository)
    private kpiRepository: KpiRepository) {
    }

    async createKpi(objId: number, createKpiDto: CreateKpiDto, user: User): Promise<Kpi> {
        const objective = await Objective.findOne(objId, { relations: ['kpi', 'company'] });
        if (!objective) throw new NotFoundException("No objective with id" + objId);
        if (!await this.permissions(objective.company.ownerId, user)) {
            throw new ForbiddenException("You dont have permissions create kpi")
        }

        const kpi = new Kpi();
        Object.assign(kpi, createKpiDto);
        await kpi.save()

        objective.kpi.push(kpi);
        await objective.save();

        return kpi;
    }

    async getKpis(): Promise<Kpi[]> {
        return await this.kpiRepository.find({ order: { id: 'DESC' } });
    }


    async getKpiById(id: number): Promise<Kpi> {
        const kpi = await this.kpiRepository.findOne(id);
        if (!kpi) throw new NotFoundException('Kpi with such id was not found');
        return kpi;
    }

    async updateKpi(id: number, updateKpiDto: UpdateKpiDto, user: User): Promise<Kpi> {

        const kpi = await this.kpiRepository.findOne({ id }, { relations: ["objective", "objective.company"] });
        if (!kpi) throw new NotFoundException("No kpi with id" + id);

        if (!await this.permissions(kpi.objective.company.ownerId, user)) {
            throw new ForbiddenException("You dont have permissions update kpi")
        }
        Object.assign(kpi, updateKpiDto);
        return kpi.save()
    }


    async deleteKpi(id: number, user: User): Promise<void> {
        const kpi = await this.kpiRepository.findOne({ id }, { relations: ["objective", "objective.company"] });
        if (!kpi) throw new NotFoundException("No kpi with id" + id);

        if (user.role !== 'superAdmin') {
            if (kpi.objective.company.ownerId !== user.id) {
                throw new ForbiddenException("You dont have permissions update kpi")
            }
        }
        await this.kpiRepository.delete({ id });
    }

    async getKpiLogs(kpiId: number): Promise<KpiLog[]> {
        return KpiLog.find({ where: { kpiId } });

    }

    private permissions = async (ownerId: number, user: User): Promise<boolean> => {
        if (user.role !== 'superAdmin') {
            if (ownerId !== user.id) {
                if (user.role !== 'collaborator') {
                    return false
                }
            }
        }
        return true
    };

}
