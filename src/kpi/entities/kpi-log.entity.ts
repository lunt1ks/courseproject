import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm/index";

@Entity()
export class KpiLog extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({
        type: "timestamp without time zone",
        default: () => 'CURRENT_TIMESTAMP', nullable: true
    })
    date:Date

    @Column()
    kpiId: number;

    @Column({nullable:true})
    current_value: number;

    @Column({nullable:true})
    units: string;
}