import {BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Objective} from "../../objectives/entities/objective.entity";
import {AfterInsert, AfterUpdate} from "typeorm/index";
import {InternalServerErrorException} from "@nestjs/common";
import {KpiLog} from "./kpi-log.entity";

@Entity()
export class Kpi extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({nullable:true})
    goal: number;

    @Column({nullable:true})
    current_value: number;

    @Column({nullable: true})
    units: string;

    @ManyToOne(type => Objective, objective => objective.kpi,
        {onDelete: "CASCADE", onUpdate: "CASCADE"})
    objective: Objective

    @AfterInsert()
    async createLogAfterInsert() {
        const log = new KpiLog();
        log.current_value = this.current_value;
        log.kpiId = this.id;
        log.units = this.units?this.units:null;

        try {
            await log.save();
        } catch (ex) {
            throw new InternalServerErrorException(`Error creating log for indicator with id ${this.id}\n
            Error:${ex.message}`);
        }
    }

    @AfterUpdate()
    async createLogBeforeUpdate() {
        const logs = await KpiLog.find({
            where: {kpiId: this.id},
            order: {
                id: 'DESC'
            },
            take: 1
        });
        if (logs.length > 0) {
            const latest = logs.pop()
            if (this.current_value === latest.current_value) return
        }
        const log = new KpiLog();
        log.current_value = this.current_value;
        log.kpiId = this.id;
        log.units = this.units;

        try {
            await log.save();
        } catch (ex) {
            throw new InternalServerErrorException(`Error creating log for indicator with id ${this.id}`);
        }
    }
}