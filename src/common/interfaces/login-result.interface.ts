
export interface LoginResult {
    id: number, 
    email: string,
    accessToken: string,
    refreshToken: string
}