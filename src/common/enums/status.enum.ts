export enum Statuses {
    onTrack = 'On Track',
    atRisk = 'At Risk',
    offTrack = 'Off Track',
    completed = 'Completed',
    notStarted = 'Not started',
    aheadOfSchedule = 'Ahead of schedule'
}