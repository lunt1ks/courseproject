import {TypeOrmModuleOptions} from '@nestjs/typeorm';
import { ConfigService } from "./config.service";

export const typeOrmConfig = (configService: ConfigService): TypeOrmModuleOptions => ({
    type: configService.get('DATABASE_DIALECT'),
    host: configService.get('DATABASE_HOST'),
    port: configService.get('DATABASE_PORT'),
    username: configService.get('DATABASE_USER'),
    password: configService.get('DATABASE_PASSWORD'),
    database: configService.get('DATABASE_NAME'),
    entities: [__dirname + '/../**/*.entity.{js,ts}', __dirname + '/../**/entities/*.entity.{js,ts}'],
    synchronize: true, //temporary
    migrationsRun: true,
    logging: true,
    logger: 'file',
    dropSchema: false,
    migrations: [__dirname + 'migration/**/*{.ts,.js}'],
    cli: {
        migrationsDir: 'src/migration',
    }

})