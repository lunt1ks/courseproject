import { ConfigService } from "./config.service";
import * as mailgunTransport from 'nodemailer-mailgun-transport';

export const mailerConfig = (configService: ConfigService): any => {
    return {
        transport: mailgunTransport({
            auth: {
                api_key: configService.get('MAILGUN_API_KEY'),
                domain: configService.get('MAILGUN_DOMAIN'),
            }
        }),
        defaults: {
            from: 'Okapi <noreply@okapi.com>',
        },
    };
};
