import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    UseGuards,
    ValidationPipe
} from '@nestjs/common';
import {ActionsService} from "./actions.service";
import {CreateActionDto} from "./dto/create-action.dto";
import {User} from "../auth/entities/user.entity";
import {GetUser} from "../auth/custom-decorators/get-user.decorator";
import {UpdateActionDto} from "./dto/update-action.dto";
import {AuthGuard} from "@nestjs/passport";
import {Action} from "./action.entity";
import {Roles} from "../auth/custom-decorators/roles.decorator";
import { RolesGuard } from 'src/auth/custom-guards/roles.guard';

@Controller('api/actions')
export class ActionsController {
    constructor(private actionsService: ActionsService) {
    }

    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Get()
    async getActions(): Promise<Action[]> {
        return await this.actionsService.getActions();
    }

    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Get('/:id')
    async getActionById(@Param('id', ParseIntPipe) id: number): Promise<Action> {
        return await this.actionsService.getActionById(id);
    }

    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Get('initiative/:initiativeId')
    async getActionsForInitiative(@Param('initiativeId', ParseIntPipe) id: number): Promise<Action[]> {
        return await this.actionsService.getActionsForInitiative(id);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('/:initiativeId')
    async createAction(@Body(ValidationPipe) createActionDto: CreateActionDto,
                       @GetUser()user: User,
                       @Param('initiativeId', ParseIntPipe) initiativeId: number): Promise<Action> {
        return await this.actionsService.createAction(createActionDto, initiativeId, user)
    }

    @Roles('superAdmin', 'admin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Patch('/:id')
    async updateAction(@Param('id', ParseIntPipe) id: number,
                       @Body(ValidationPipe) updateActionDto: UpdateActionDto,
                       @GetUser() user: User): Promise<Action> {
        return await this.actionsService.updateAction(updateActionDto, id, user)
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete('/:id')
    async deleteAction(@Param('id', ParseIntPipe) id: number,
                       @GetUser() user: User): Promise<void> {
        return await this.actionsService.deleteAction(id, user)
    }
}
