import {BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Initiative} from "../initiatives/initiative.entity";
import {User} from "../auth/entities/user.entity";

@Entity()
export class Action extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type:"text"})
    status: string;

    @Column()
    note:string;
    @Column({type: "timestamp without time zone",
        default: () => 'CURRENT_TIMESTAMP', nullable:true})
    create_date:string;

    @Column()
    ownerId: number;

    @ManyToOne(type=>Initiative, initiative=>initiative.actions,
        {onDelete:"CASCADE", onUpdate:"CASCADE"})
    initiative:Initiative;

    @ManyToOne(type=>User, user=>user.actions, {cascade:false})
    user:User;
}