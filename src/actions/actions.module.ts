import {Module} from '@nestjs/common';
import {ActionsController} from './actions.controller';
import {ActionsService} from './actions.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {CompanyRepository} from "../companies/company.repository";
import {ActionRepository} from "./action.repository";
import {AuthModule} from "../auth/auth.module";

@Module({
    imports: [TypeOrmModule.forFeature([ActionRepository]),
    AuthModule],
    controllers: [ActionsController],
    providers: [ActionsService]
})
export class ActionsModule {
}
