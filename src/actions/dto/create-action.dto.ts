import {IsIn, IsNotEmpty, MaxLength, MinLength} from "class-validator";
import {Statuses} from "../../common/enums/status.enum";

export class CreateActionDto {

    @IsNotEmpty()
    @IsIn([
        Statuses.atRisk,
        Statuses.onTrack,
        Statuses.offTrack,
        Statuses.completed,
        Statuses.aheadOfSchedule,
        Statuses.notStarted
    ])
    status:string

    @IsNotEmpty()
    @MinLength(2)
    @MaxLength(255)
    note:string
}