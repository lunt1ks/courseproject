import {IsIn, IsNotEmpty, IsNumber, IsOptional, MaxLength, MinLength} from "class-validator";
import {Statuses} from "../../common/enums/status.enum";

export class UpdateActionDto {
    @IsNotEmpty()
    @IsOptional()
    @IsIn([
        Statuses.atRisk,
        Statuses.onTrack,
        Statuses.offTrack,
        Statuses.completed,
        Statuses.notStarted,
        Statuses.aheadOfSchedule
    ])
    status:string

    @IsNotEmpty()
    @MinLength(2)
    @MaxLength(255)
    @IsOptional()
    note:string

    @IsOptional()
    @IsNumber()
    ownerId: number;
}
