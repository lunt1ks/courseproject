import {ForbiddenException, Injectable, NotFoundException} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {ActionRepository} from "./action.repository";
import {CreateActionDto} from "./dto/create-action.dto";
import {Action} from "./action.entity";
import {Initiative} from "../initiatives/initiative.entity";
import {UpdateActionDto} from "./dto/update-action.dto";
import {User} from "../auth/entities/user.entity";
import {Objective} from "../objectives/entities/objective.entity";

@Injectable()
export class ActionsService {
    constructor(@InjectRepository(ActionRepository)
                private actionRepository: ActionRepository) {
    }

    async getActions(): Promise<Action[]> {
        return this.actionRepository.find();
    }

    async getActionsForInitiative(initiativeId: number): Promise<Action[]> {
        const initiative = await Initiative.findOne(initiativeId);
        if (!initiative) throw new NotFoundException("Initiative with id " + initiativeId + "was not found");

        return initiative.actions;
    }

    async getActionById(id: number) {
        return await this.actionRepository.findOne(id);
    }

    async createAction(createActionDto: CreateActionDto, initiativeId: number, user: User): Promise<Action> {
        const initiative = await Initiative.findOne(initiativeId, {relations: ['objective', 'objective.users', 'objective.company']});
        if (!initiative) throw new NotFoundException(`Initiative with id ${initiativeId} was not found`);
        if (user.role !== 'superAdmin') {
            if (!initiative.objective.users.some(item => item.id === user.id)) {
                throw new ForbiddenException('You don\'t have permissions to create actions in that initiative')
            }
        }
        
        const action: Action = new Action();
        user.role === 'superAdmin' ? action.ownerId = initiative.objective.company.ownerId : action.ownerId = user.id;
        Object.assign(action, createActionDto);
        action.initiative = initiative;
        action.user = user;
        return await action.save();
    }

    async updateAction(updateActionDto: UpdateActionDto, id: number, user: User): Promise<Action> {
        const action = await this.actionRepository.findOne(id, {relations: ['initiative', "initiative.objective", "initiative.objective.users"]});
        if (!action) throw new NotFoundException('Action with id ' + id + 'was not found');
        if (!action.initiative.objective.users.some(item => user.id === item.id)) {

                throw new ForbiddenException('You don\'t have permissions to update actions in that initiative')

        }

        this.actionRepository.createQueryBuilder('actions')

        if (!updateActionDto.status) {
            updateActionDto.status = action.status;
        }

        if (!updateActionDto.note) {
            updateActionDto.note = action.note;
        }

        if (!updateActionDto.ownerId) {
            updateActionDto.ownerId = action.ownerId;
        }

        Object.assign(action, updateActionDto);
        return await action.save();
    }

    async deleteAction(id: number, user: User): Promise<void> {
        const action = await this.actionRepository.findOne(id, {relations: ['initiative', "initiative.objective", "initiative.objective.users"]});
        if (!action) throw new NotFoundException('Action with id ' + id + 'was not found');
        if (user.role !== 'superAdmin') {
            if (action.initiative.owner_id !== user.id) {
                throw new ForbiddenException('You don\'t have permissions to delete actions in that initiative')
            }
        }
        await this.actionRepository.delete({id});
    }

    private permissions = async (ownerId: number, user: User, action: Action) => {
        if (action.initiative.objective.users.some(user => user.id)) return true

        if (user.role !== 'superAdmin') {
            if (ownerId !== user.id) {
                if (user.role !== 'collaborator') {
                    return false
                }
            }
        }
        return true
    };


}
