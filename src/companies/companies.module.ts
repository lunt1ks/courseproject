import { Module } from '@nestjs/common';
import { CompaniesController } from './companies.controller';
import { CompanyService } from './companies.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {CompanyRepository} from "./company.repository";
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [
    AuthModule,
    TypeOrmModule.forFeature([CompanyRepository])
  ],

  controllers: [
    CompaniesController,
  ],

  providers: [
    CompanyService
  ],
})
export class CompaniesModule {}
