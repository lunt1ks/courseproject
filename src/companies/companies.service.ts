import {
    Injectable,
    NotFoundException,
    InternalServerErrorException,
    ConflictException,
    ForbiddenException
} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Company} from "./company.entity";
import {CompanyRepository} from "./company.repository";
import {CreateCompanyDto} from "./dto/create-company.dto";
import {UpdateCompanyDto} from "./dto/update-company.dto";
import {User} from 'src/auth/entities/user.entity';
import {IndicatorLog} from 'src/indicators/entities/indicator-log.entity';
import {ObjectiveLog} from "../objectives/entities/objective-log.entity";
import {KpiLog} from "../kpi/entities/kpi-log.entity";
import { Invite } from 'src/invite/invite.entity';

const uniqueConstraintCode = 23505;

@Injectable()
export class CompanyService {

    constructor(
        @InjectRepository(CompanyRepository)
        private companyRepository: CompanyRepository
    ) {
    }

    async createCompany(createCompanyDto: CreateCompanyDto, userId: number): Promise<Company> {
        let company = await Company.findOne({where: {ownerId: userId}});
        if (company) throw new ConflictException('You already have company');
        company = new Company();
        Object.assign(company, createCompanyDto);
        company.ownerId = userId;
        const owner = await User.findOne(userId);
        company.users = [];
        company.objectives = [];
        company.indicators = [];
        company.users.push(owner);
        try {
            return await company.save();
        } catch (ex) {
            if (ex.code == uniqueConstraintCode) {
                throw new ConflictException(`Company with such name already exists`);
            }
            throw new InternalServerErrorException(`Error creating company: ${ex.message}`);
        }
    }

    async getCompanies(): Promise<Company[]> {
        return await this.companyRepository.createQueryBuilder('company')
            .leftJoinAndSelect('company.objectives', 'objective')
            .leftJoinAndSelect('company.users', 'users')
            .leftJoinAndSelect('objective.initiatives', 'initiative')
            .leftJoinAndSelect('objective.kpi', 'kpi')
            .leftJoinAndSelect('objective.users', 'user')
            .leftJoinAndSelect('initiative.actions', 'action')
            .leftJoinAndSelect('company.indicators', 'indicators')
            .orderBy({
                'initiative.id': 'ASC',
                'objective.id': 'ASC',
                'kpi.id': 'ASC',
                'action.create_date': 'ASC'
            })
            .getMany();
    }

    async getCompanyForAdmin(user: User): Promise<any> {
        const company = await this.companyRepository.findOne({where: {ownerId: user.id}, select: ['id', 'name','updateAction', 'updateKpi'], relations: ['users']});

        if(!company) {
            throw new NotFoundException(`Company, owner by user with id ${user.id} was not found`);
        }

        const invites = await Invite.find( { where: {companyId: company.id}});

        const pending = {
            invites
        };

        Object.assign(company, pending);
        
        company.users.sort(function (a,b) {
            return a.id - b.id;
        });

        return company;
    }

    async getCompanyById(userId: number, id: number): Promise<any> {
        const company = await this.companyRepository.createQueryBuilder('company')
            .leftJoinAndSelect('company.objectives', 'objective')
            .leftJoinAndSelect('company.users', 'users')
            .leftJoinAndSelect('objective.initiatives', 'initiative')
            .leftJoinAndSelect('objective.kpi', 'kpi')
            .leftJoinAndSelect('objective.users', 'user')
            .leftJoinAndSelect('initiative.actions', 'action')
            .leftJoinAndSelect('company.indicators', 'indicators')
            .orderBy({
                'initiative.id': 'ASC',
                'objective.id': 'DESC',
                'kpi.id': 'ASC',
                'action.create_date': 'ASC'
            })
            .where('company.id = :id', {id})
            .getOne();
            
            if (!company) {
                throw new NotFoundException('Company with such id was not found');
            }
        
        const invites = await Invite.find( {where: {companyId: id}});
        
        const pending = {
            invites
        };

        Object.assign(company, pending);

        company.objectives.forEach(item => {
            item.kpi.sort(function (a,b) {
                return b.id - a.id;
            });
        })

        company.objectives.sort(function (a, b) {
            return b.id - a.id;
        });

        company.indicators.sort(function (a, b) {
            return b.id - a.id;
        });
        
        company.users.sort(function(a,b) {
            return a.id - b.id;
        });

        for (const indicator of company.indicators) {
            const logs = await IndicatorLog.find( { where: {indicatorId: indicator.id}});

            const log = {
                logs
            };

            Object.assign(indicator, log);
        }
        company.objectives.forEach((item) => {
            const donut = {
                donut: {
                    onTrack: 0,
                    atRisk: 0,
                    offTrack: 0,
                    completed: 0,
                    notStarted: 0,
                    aheadOfSchedule: 0
                }
            };
            for (const initiative of item.initiatives) {
                if (initiative.actions.length < 1) {
                    donut.donut.notStarted++;
                    continue;
                }
                switch (initiative.actions[initiative.actions.length - 1].status) {
                    case 'On Track':
                        donut.donut.onTrack++;
                        break;
                    case 'At Risk':
                        donut.donut.atRisk++;
                        break;
                    case 'Off Track':
                        donut.donut.offTrack++;
                        break;
                    case 'Completed':
                        donut.donut.completed++;
                        break;
                    case 'Not started':
                        donut.donut.notStarted++;
                        break;
                    case 'Ahead of schedule':
                        donut.donut.aheadOfSchedule++;
                        break;
                    default:
                        break;
                }
            }
            Object.assign(item, donut)
        });

        for (const objective of company.objectives) {
            const objLogs = await ObjectiveLog.find({where: {objectiveId: objective.id}});
            const objLog = {
                objLogs
            }

            Object.assign(objective, objLog);
            for(const kpi of objective.kpi){
                const kpiLogs = await KpiLog.find({where: {kpiId: kpi.id}});
                const kpiLog = {
                    kpiLogs
                }
                Object.assign(kpi, kpiLog)
            }
        }

        return company;
    }


    async getMyCompany({id}: User): Promise<Company> {
        const company = await this.companyRepository.createQueryBuilder('company')
            .innerJoinAndSelect('company.users', 'user')
            .where('user.id = :id', {id})
            .getOne();
        if (!company) return;
        const myCompany = await this.companyRepository.createQueryBuilder('company')
            .leftJoinAndSelect('company.objectives', 'objective')
            .leftJoinAndSelect('company.users', 'users')
            .leftJoinAndSelect('objective.initiatives', 'initiative')
            .leftJoinAndSelect('objective.kpi', 'kpi')
            .leftJoinAndSelect('objective.users', 'user')
            .leftJoinAndSelect('initiative.actions', 'action')
            .leftJoinAndSelect('company.indicators', 'indicators')
            .orderBy({
                'initiative.id': 'ASC',
                'objective.id': 'DESC',
                'kpi.id': 'ASC',
                'action.create_date': 'ASC'
            })
            .where('company.id = :id', {id: company.id})
            .getOne()
        
        myCompany.objectives.forEach(item => {
            item.kpi.sort(function (a,b) {
                return b.id - a.id;
            });
        })

        myCompany.users.sort(function (a, b) {
            return a.id - b.id;
        });
        myCompany.objectives.sort(function (a, b) {
            return b.id - a.id;
        });
        myCompany.indicators.sort(function (a, b) {
            return b.id - a.id;
        });

        myCompany.objectives.forEach((item) => {
            const donut = {
                donut: {
                    onTrack: 0,
                    atRisk: 0,
                    offTrack: 0,
                    completed: 0,
                    notStarted: 0,
                    aheadOfSchedule: 0
                }
            };
            for (const initiative of item.initiatives) {
                if (initiative.actions.length < 1) {
                    donut.donut.notStarted++;
                    continue;
                }
                switch (initiative.actions[initiative.actions.length - 1].status) {
                    case 'On Track':
                        donut.donut.onTrack++;
                        break;
                    case 'At Risk':
                        donut.donut.atRisk++;
                        break;
                    case 'Off Track':
                        donut.donut.offTrack++;
                        break;
                    case 'Completed':
                        donut.donut.completed++;
                        break;
                    case 'Not started':
                        donut.donut.notStarted++;
                        break;
                    case 'Ahead of schedule':
                        donut.donut.aheadOfSchedule++;
                        break;
                    default:
                        break;
                }
            }
            Object.assign(item, donut)
        });

        for (const indicator of myCompany.indicators) {
            const indLogs = await IndicatorLog.find({where: {indicatorId: indicator.id}});
            const indLog = {
                indLogs
            }
            Object.assign(indicator, indLog);
        }
        for (const objective of myCompany.objectives) {
            const objLogs = await ObjectiveLog.find({where: {objectiveId: objective.id}});
            const objLog = {
                objLogs
            }

            Object.assign(objective, objLog);
            for(const kpi of objective.kpi){
                const kpiLogs = await KpiLog.find({where: {kpiId: kpi.id}});
                const kpiLog = {
                    kpiLogs
                }
                Object.assign(kpi, kpiLog)
            }
        }
        return myCompany;
    }


    async updateKpi(companyId: number, user: User, value: boolean): Promise<Company> {
        const company = await Company.findOne(companyId)
        if (user.role !== 'superAdmin') {
            if (company.ownerId !== user.id) {
                throw new ForbiddenException("You dont have permissions to remove users");
            }
        }

        company.updateKpi = value;
        return await company.save()
    }

    async updateActions(companyId: number, user: User, value: boolean): Promise<Company> {
        const company = await Company.findOne(companyId)
        if (user.role !== 'superAdmin') {
            if (company.ownerId !== user.id) {
                throw new ForbiddenException("You dont have permissions to remove users");
            }
        }

        company.updateAction = value;
        return await company.save()
    }

    async removeUserFromCompany(userId: number, companyId: number, owner: User) {
        const company = await this.companyRepository.findOne(companyId, {relations: ['users']});
        if (!company) throw new NotFoundException('No company with such id');
        if (owner.role !== 'superAdmin') {
            if (company.ownerId !== owner.id) {
                throw new ForbiddenException("You dont have permissions to remove users");
            }
        }
        const exists = company.users.some(user => user.id === userId);
        if (!exists) throw new NotFoundException('There is not such user in this company');
        company.users = company.users.filter(item => item.id !== userId);
        await (await User.findOne(userId)).remove();
        return await company.save();
    }

    async updateCompany(id: number, updateCompanyDto: UpdateCompanyDto, user: User): Promise<Company> {
        const company = await this.companyRepository.findOne({id});
        if (user.role !== 'superAdmin') {
            if (company.ownerId !== user.id) {
                throw new ForbiddenException('You dont have permissions to update company')
            }
        }
        Object.assign(company, updateCompanyDto);
        return await company.save();
    }

    async deleteCompany(id: number, user: User): Promise<void> {
        const company = await this.companyRepository.findOne(id)
        if (user.role !== 'superAdmin') {
            if (company.ownerId !== user.id) {
                throw new ForbiddenException('You dont have permissions to update company')
            }
        }
        try {
            await this.companyRepository.delete(company);
        } catch (ex) {
            throw new InternalServerErrorException(`An error deleting company: ${ex.message}`);
        }
    }
}
