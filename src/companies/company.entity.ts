import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn, Unique, ManyToMany, JoinTable} from "typeorm";
import {Objective} from "../objectives/entities/objective.entity";
import { User } from "src/auth/entities/user.entity";
import { Indicator } from "src/indicators/entities/indicator.entity";

@Entity()
export class Company extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string;

    @Column({ nullable: true })
    avatar: string;

    @Column({ nullable: true })
    mission: string

    @Column({ nullable: true , default:false})
    updateKpi: boolean

    @Column({ nullable: true , default:false})
    updateAction: boolean

    @OneToMany(type => Objective, objective => objective.company, {cascade: true})
    objectives: Objective[];
    
    @ManyToMany(type => User, user => user.companies, {cascade: false})
    @JoinTable()
    users: User[];

    @OneToMany(type => Indicator, indicator => indicator.company, {cascade: true})
    indicators: Indicator[];

    @Column()
    ownerId: number;
}
