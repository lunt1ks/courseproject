import {IsDate, IsNotEmpty, IsOptional, IsString, MinLength} from "class-validator";

export class CreateCompanyDto {

    @IsNotEmpty()
    @IsOptional()
    avatar:string

    @IsOptional()
    mission:string

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    name: string;
}
