import {IsNotEmpty, IsOptional, IsString } from "class-validator";

export class UpdateCompanyDto{

    @IsNotEmpty()
    @IsOptional()
    avatar:string

    @IsOptional()
    mission:string

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    name: string;

}