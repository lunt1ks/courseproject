import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    ValidationPipe,
    UseGuards, ParseBoolPipe
} from '@nestjs/common';
import {CompanyService} from "./companies.service";
import {CreateCompanyDto} from "./dto/create-company.dto";
import {UpdateCompanyDto} from "./dto/update-company.dto";
import {Company} from './company.entity';
import {GetUser} from 'src/auth/custom-decorators/get-user.decorator';
import {User} from 'src/auth/entities/user.entity';
import {AuthGuard} from '@nestjs/passport';
import {RolesGuard} from 'src/auth/custom-guards/roles.guard';
import {Roles} from 'src/auth/custom-decorators/roles.decorator';

@UseGuards(AuthGuard('jwt'), RolesGuard)
@Controller('api/companies')
export class CompaniesController {
    constructor(
        private companyService: CompanyService
    ) {
    }

    @Roles('superAdmin')
    @Get('/get/all')
    async getAllCompanies(): Promise<Company[]> {
        return await this.companyService.getCompanies();
    }

    @Roles('superAdmin')
    @Get('/get/:id')
    async getCompanyById(@GetUser() user: User,
                         @Param('id', ParseIntPipe) id: number
    ): Promise<any> {
        return await this.companyService.getCompanyById(user.id, id);
    }
    
    @Roles('admin', 'superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Get('/admin/')
    async getCompanyForAdmin(
        @GetUser() user: User,
    ): Promise<any> {
        return this.companyService.getCompanyForAdmin(user);
    }

    @Get('/me')
    async getMyCompany(
        @GetUser() user: User,
    ): Promise<Company> {
        return this.companyService.getMyCompany(user);
    }

    @Roles('superAdmin', 'admin')
    @Delete('/user/remove/:companyId')
    async removeUserFromCompany(
        @Body('userId', ParseIntPipe) userId: number,
        @Param('companyId', ParseIntPipe) companyId: number,
        @GetUser() owner: User,
    ): Promise<Company> {
        return this.companyService.removeUserFromCompany(userId, companyId, owner);
    }

    @Roles('superAdmin', 'admin')
    @Patch('/update/kpi/:companyId')
    async updateKpi(@Param('companyId', ParseIntPipe)companyId: number,
                    @Body('value', ParseBoolPipe) value: boolean,
                    @GetUser() owner: User,
    ): Promise<Company> {
        return this.companyService.updateKpi(companyId,owner, value);
    }

    @Roles('superAdmin', 'admin')
    @Patch('/update/actions/:companyId')
    async updateActions(@Param('companyId', ParseIntPipe)companyId: number,
        @Body('value', ParseBoolPipe) value: boolean,
        @GetUser() owner: User,
    ): Promise<Company> {
        return this.companyService.updateActions(companyId,owner, value);
    }


    @Roles('superAdmin', 'admin')
    @Post('/create')
    async createCompany(
        @GetUser() user: User,
        @Body(ValidationPipe) createCompanyDto: CreateCompanyDto
    ): Promise<Company> {
        return await this.companyService.createCompany(createCompanyDto, user.id);
    }

    @Roles('superAdmin', 'admin')
    @Patch('/:id')
    async updateCompany(
        @Param('id', ParseIntPipe) id: number,
        @Body(ValidationPipe) updateCompanyDto: UpdateCompanyDto,
        @GetUser() user: User
    ): Promise<Company> {
        return await this.companyService.updateCompany(id, updateCompanyDto, user)
    }

    @Roles('superAdmin', 'admin')
    @Delete(':id')
    async deleteCompany(
        @Param('id', ParseIntPipe,) id: number,
        @GetUser() user: User,
    ): Promise<void> {
        return await this.companyService.deleteCompany(id, user)
    }
}
