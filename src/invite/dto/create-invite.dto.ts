import {IsEmail, IsIn, IsNotEmpty} from "class-validator";

export class CreateInviteDto {
    @IsEmail()
    @IsNotEmpty()
    email: string

    @IsNotEmpty()
    @IsIn(["contributor", "collaborator"])
    role: string
}