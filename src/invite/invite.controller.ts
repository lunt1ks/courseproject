import {Body, Controller, Get, Param, ParseIntPipe, Post, UseGuards, ValidationPipe} from '@nestjs/common';
import {InviteService} from "./invite.service";
import {isEmail} from "class-validator";
import {AuthGuard} from "@nestjs/passport";
import {GetUser} from "../auth/custom-decorators/get-user.decorator";
import {User} from "../auth/entities/user.entity";
import {Roles} from "../auth/custom-decorators/roles.decorator";
import { RolesGuard } from 'src/auth/custom-guards/roles.guard';
import {CreateInviteDto} from "./dto/create-invite.dto";

@Controller('api/invite')
export class InviteController {
    constructor(private inviteService: InviteService) {
    }

    @Roles('superAdmin', 'admin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Post('/user/:companyId')
    async inviteToCompany(@Param('companyId', ParseIntPipe) companyId: number,
                          @Body(ValidationPipe) createInviteDto: CreateInviteDto,
                          @GetUser() user: User) {
        return await this.inviteService.inviteUserToCompany(companyId, createInviteDto, user)
    }


    @Roles('superAdmin', 'admin')
    @UseGuards(AuthGuard('jwt'))
    @Get('/getAll/:companyId')
    async getInvites(
        @Param('companyId', ParseIntPipe) companyId: number,
        @GetUser() user: User) {
        return await this.inviteService.getInvitesForCompany(user, companyId)
    }

}
