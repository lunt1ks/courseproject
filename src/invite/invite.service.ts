import {
    BadRequestException,
    ConflictException,
    ForbiddenException,
    Injectable,
    NotFoundException
} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {InviteRepository} from "./invite.repository";
import {User} from "../auth/entities/user.entity";
import {Company} from "../companies/company.entity";
import {Invite} from "./invite.entity";
import {isEmail} from "class-validator";
import {MailSenderService} from 'src/mailer/mail-sender.service';
import {CreateInviteDto} from "./dto/create-invite.dto";

@Injectable()
export class InviteService {
    constructor(
        @InjectRepository(InviteRepository)
        private contentRepository: InviteRepository,
        private mailService: MailSenderService
    ) {

    };

    async inviteUserToCompany(companyId: number, createInviteDto: CreateInviteDto, owner: User): Promise<Invite> {
        const user = await User.findOne({email: createInviteDto.email}, {relations:['companies']});
        console.log(user);
        if (user && user.companies) throw new ConflictException('User already has company');
        const company = await Company.findOne(companyId);
        if (!isEmail(createInviteDto.email)) throw new BadRequestException('Email is not valid');
        if (!company) throw new NotFoundException('No company with id ' + companyId);

        if (owner.role !== 'superAdmin') {
            if (company.ownerId !== owner.id) {
                throw new ForbiddenException('Ypu dont have permissions to add user to this company');
            }
        }
        let invite = await Invite.findOne({where:{email:createInviteDto.email}});
        if(invite) throw new ConflictException('User is already has invite');
        invite = new Invite()
        Object.assign(invite, createInviteDto);
        invite.companyId = companyId;


        this.mailService.sendCustomEmail(createInviteDto.email, {
            subject: 'Invitation to Okapi', text: `You have been invited to join ${company.name}!\n
         Follow the link: http://ec2-13-48-126-108.eu-north-1.compute.amazonaws.com`
        });

        return await invite.save()
    }

    async getInvitesForCompany(user: User, companyId: number): Promise<Invite[]> {
        const company = await Company.findOneOrFail(companyId);
        if (user.role !== 'superAdmin') {
            if (company.ownerId !== user.id) {
                throw new ForbiddenException('Ypu dont have permissions to see invites');

            }
        }
        return await Invite.find({where: {companyId}});
    }
}