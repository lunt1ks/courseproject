import { Module } from '@nestjs/common';
import { InviteService } from './invite.service';
import { InviteController } from './invite.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {InviteRepository} from "./invite.repository";
import { MailSenderService } from 'src/mailer/mail-sender.service';
import { MailerModule } from '@nest-modules/mailer';
import { ConfigModule } from 'src/config/config.module';
import { ConfigService } from 'src/config/config.service';
import { mailerConfig } from 'src/config/mailer.config';

@Module({
  imports:[
    TypeOrmModule.forFeature([InviteRepository]),
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return mailerConfig(configService);
      },
      inject: [ConfigService]
    })
  ],
  providers: [
    InviteService,
    MailSenderService
  ],
  controllers: [InviteController]
})
export class InviteModule {}

