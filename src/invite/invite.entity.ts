import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";


@Entity()

export class Invite extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({type: "text"})
    email: string;
    @Column({type: "integer"})
    companyId: number;
    @Column({type: "text", default:'contributor'})
    role: string;
    @Column({
        type: "timestamp without time zone",
        default: () => 'CURRENT_TIMESTAMP', nullable: true
    })
    create_date: Date
}