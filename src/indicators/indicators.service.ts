import {Injectable, ForbiddenException, NotFoundException, InternalServerErrorException} from '@nestjs/common';
import {CreateIndicatorDto} from './dto/create-indicator.dto';
import {Indicator} from './entities/indicator.entity';
import {User} from 'src/auth/entities/user.entity';
import {Company} from 'src/companies/company.entity';
import {IndicatorRepository} from './indicator.repository';
import {UpdateIndicatorDto} from './dto/update-indicator.dto';
import {IndicatorLog} from './entities/indicator-log.entity';

@Injectable()
export class IndicatorsService {

    constructor(
        private readonly indicatorRepository: IndicatorRepository,
    ) {
    }

    async getCompanyIndicators(user: User, companyId: number): Promise<any> {

        const company = await Company.findOne(companyId, {relations: ['users']});

        if (!company) {
            throw new NotFoundException(`Company with id ${companyId} was not found`);
        } else {
            if (!this.checkMembership(company.ownerId, user)) {
                throw new ForbiddenException(`You don't have enough permissions`);
            }
            const indicators = await this.indicatorRepository.find({
                relations: ['company'],
                where: {company: {id: companyId}}
            });

            const logs = [];

            for (const indicator of indicators) {
                const indicatorLog = await IndicatorLog.findOne({where: {indicatorId: indicator.id}});
                if (indicatorLog) {
                    logs.push(indicatorLog);
                }
            }
            return {
                indicators,
                logs,
                company: {
                    companyId: company.id,
                    companyName: company.name,
                    companyUsers: company.users
                }
            }
        }
    }

    async getIndicatorById(user: User, companyId: number, indicatorId: number) {

        const company = await Company.findOne(companyId, {relations: ['users']});

        if (!company) {
            throw new NotFoundException(`No company with id ${companyId} was found`);
        }

        if (!this.checkMembership(company.ownerId, user)) {
            throw new ForbiddenException(`You don't have enough permissions`);
        }

        const indicator = await this.indicatorRepository.findOne(indicatorId, {where: {company: {id: companyId}}});

        if (!indicator) {
            throw new NotFoundException(`Indicator with id ${indicatorId} was not found for the company with id ${companyId}`);
        }

        const logs = await IndicatorLog.find({where: {indicatorId}});

        return {
            foundIndicator: indicator,
            logs,
            company: {
                companyId: company.id,
                companyName: company.name,
                companyUsers: company.users
            }
        }
    }

    async getLogs(companyId: number, indicatorId: number, user: User): Promise<IndicatorLog[]> {
        const foundCompany = await Company.findOne(companyId, {relations: ['users']});
        if (!foundCompany) {
            throw new NotFoundException(`Company with id ${companyId} was not found`);
        }
        if (!this.checkMembership(foundCompany.ownerId, user)) {
            throw new ForbiddenException(`You don't have enough permissions to retrieve logs`);
        }

        return await IndicatorLog.find({where: {indicatorId}});
    }

    async getAllIndicators(): Promise<Indicator[]> {
        return await this.indicatorRepository.find();
    }


    async createIndicator(user: User, companyId: number, createIndicatorDto: CreateIndicatorDto): Promise<Indicator> {

        const foundUser = await User.findOne(user.id);


        if (!foundUser) {
            throw new ForbiddenException(`There is no user with email ${user.email} registered`);
        }

        const foundCompany = await Company.findOne(companyId, {relations: ['indicators', 'users']});

        if (!foundCompany) {
            throw new NotFoundException(`Company with id ${companyId} was not found`);
        } else {

            if (!this.checkMembership(foundCompany.ownerId, foundUser)) {
                throw new ForbiddenException(`You don't have enough permissions`);
            }

            const indicator = new Indicator();

            Object.assign(indicator, createIndicatorDto);

            foundCompany.indicators.push(indicator);

            try {
                await foundCompany.save()

                return await indicator.save();
            } catch (ex) {
                throw new InternalServerErrorException(`An error occured: ${ex.message}`);
            }
        }
    }

    async updateIndicator(user: User, companyId: number, indicatorId: number, updateIndicatorDto: UpdateIndicatorDto): Promise<Indicator> {

        if (!updateIndicatorDto) {
            return null;
        }

        const company = await Company.findOne(companyId, {relations: ['users']});

        const foundIndicator = await this.indicatorRepository.findOne(indicatorId, {where: {company: {id: companyId}}});

        if (!foundIndicator) {
            throw new NotFoundException(`Indicator with id ${indicatorId} was not found for the company with id ${companyId}`);
        }

        if (!this.checkMembership(company.ownerId, user)) {
            throw new ForbiddenException(`You don't have enough permissions`);
        }

        Object.assign(foundIndicator, updateIndicatorDto);

        return await foundIndicator.save();
    }

    async deleteIndicator(user: User, companyId: number, indicatorId: number): Promise<void> {
        const found = await this.indicatorRepository.findOne(indicatorId);

        if (!found) {
            throw new NotFoundException(`No indicator with id ${indicatorId} was found`);
        }

        const company = await Company.findOne(companyId, {relations: ['users']});

        if (!found) {
            throw new NotFoundException(`There is no company with id ${companyId}`);
        }

        if (!this.checkMembership(company.ownerId, user)) {
            throw new ForbiddenException(`You don't have enough permissions`);
        }

        try {
            await found.remove();
        } catch (ex) {
            throw new InternalServerErrorException(`Error deleting indicator: ${ex.message}`);
        }
    }


    /**
     *
     * @param ownerId
     * @param user User entity to check permissions of
     */

    private checkMembership = (ownerId: number, user: User): boolean => {
        if (ownerId !== user.id) {
            if (user.role !== 'collaborator') {
                if (user.role !== 'superAdmin') {
                    return false;
                }
            }
        }

        return true;
    }
}
