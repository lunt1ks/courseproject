import { IsString, IsOptional, IsNotEmpty, IsNumber } from "class-validator";

export class UpdateIndicatorDto {

    @IsOptional()
    @IsString()
    @IsNotEmpty()
    name: string;

    @IsOptional()
    @IsNumber()
    current_value: number;

    @IsOptional()
    @IsNumber()
    goal: number;

    @IsOptional()
    @IsString()
    units: string;
}