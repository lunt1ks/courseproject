import { IsString, IsNotEmpty, IsNumber, IsOptional } from "class-validator";

export class CreateIndicatorDto {

    @IsString()
    @IsNotEmpty()
    name: string;

    @IsNumber()
    @IsNotEmpty()
    goal: number;

    @IsNumber()
    @IsNotEmpty()
    current_value: number;

    @IsOptional()
    @IsString()
    units: string;
}