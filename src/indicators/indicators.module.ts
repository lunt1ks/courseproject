import { Module } from '@nestjs/common';
import { IndicatorsController } from './indicators.controller';
import { IndicatorsService } from './indicators.service';
import { AuthModule } from 'src/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IndicatorRepository } from './indicator.repository';

@Module({
  
  imports: [
    AuthModule,
    TypeOrmModule.forFeature([IndicatorRepository])
  ],

  controllers: [
    IndicatorsController
  ],
  providers: [
    IndicatorsService
  ],

})
export class IndicatorsModule {}
