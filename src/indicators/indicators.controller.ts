import { Controller, ValidationPipe, Body, Post, Param, ParseIntPipe, UseGuards, Get, Patch, Delete } from '@nestjs/common';
import { Indicator } from './entities/indicator.entity';
import { IndicatorsService } from './indicators.service';
import { GetUser } from 'src/auth/custom-decorators/get-user.decorator';
import { User } from 'src/auth/entities/user.entity';
import { CreateIndicatorDto } from './dto/create-indicator.dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/custom-guards/roles.guard';
import { Roles } from 'src/auth/custom-decorators/roles.decorator';
import { UpdateIndicatorDto } from './dto/update-indicator.dto';
import { IndicatorLog } from './entities/indicator-log.entity';

@Controller('api/indicators')
@UseGuards(AuthGuard('jwt'), RolesGuard)
export class IndicatorsController {

    constructor(
        private readonly indicatorService: IndicatorsService
    ) {}
    
    @Roles('admin')
    @Get('/:companyId/all')
    async getCompanyIndicators(
        @GetUser() user: User,
        @Param('companyId', ParseIntPipe) companyId: number,
    ): Promise<Indicator[]> {
        return this.indicatorService.getCompanyIndicators(user, companyId);
    }

    @Roles('superAdmin')
    @Get('/all')
    async getAllIndicators(
    ): Promise<Indicator[]> {
        return this.indicatorService.getAllIndicators();
    }

    @Get(':companyId/:indicatorId')
    async getIndicatorById(
        @GetUser() user: User,
        @Param('companyId', ParseIntPipe) companyId: number,
        @Param('indicatorId', ParseIntPipe) indicatorId: number,
    ): Promise<any> {
        return this.indicatorService.getIndicatorById(user, companyId, indicatorId);
    }

    @Get(':companyId/logs/:indicatorId')
    async getLogs(
        @GetUser() user: User,
        @Param('companyId', ParseIntPipe) companyId: number,
        @Param('indicatorId', ParseIntPipe) indicatorId: number,
    ): Promise<IndicatorLog[]> {
        return this.indicatorService.getLogs(companyId, indicatorId, user);
    }

    @Post('/:companyId')
    async createIndicator(
        @GetUser() user: User,
        @Param('companyId', ParseIntPipe) companyId: number,
        @Body(ValidationPipe) createIndicatorDto: CreateIndicatorDto
    ): Promise<Indicator> {
        return this.indicatorService.createIndicator(user, companyId, createIndicatorDto);
    }

    @Patch('/:companyId/:indicatorId')
    async updateIndicator(
        @GetUser() user: User,
        @Param('companyId', ParseIntPipe) companyId: number,
        @Param('indicatorId', ParseIntPipe) indicatorId: number,
        @Body(ValidationPipe) updateIndicatorDto: UpdateIndicatorDto,
    ): Promise<Indicator> {
        return this.indicatorService.updateIndicator(user, companyId, indicatorId, updateIndicatorDto);
    }

    @Delete(':companyId/:indicatorId')
    async deleteIndicator(
        @GetUser() user: User,
        @Param('companyId', ParseIntPipe) companyId: number,
        @Param('indicatorId', ParseIntPipe) indicatorId: number,
    ): Promise<void> {
        return this.indicatorService.deleteIndicator(user, companyId, indicatorId);
    }
}
