import {BaseEntity, PrimaryGeneratedColumn, Column, Entity, ManyToOne, AfterInsert, AfterUpdate} from "typeorm";
import {Company} from "src/companies/company.entity";
import {IndicatorLog} from "./indicator-log.entity";
import {InternalServerErrorException} from "@nestjs/common";

@Entity()
export class Indicator extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({nullable:true})
    goal: number;

    @Column({nullable:true})
    current_value: number;

    @Column({nullable: true})
    units: string;

    @ManyToOne(type => Company, company => company.indicators, {eager: true})
    company: Company;

    @AfterInsert()
    async createLogAfterInsert() {
        const log = new IndicatorLog();
        log.goal = this.goal;
        log.current_value = this.current_value;
        log.indicatorId = this.id;
        log.units = this.units ? this.units : null;

        try {
            await log.save();
        } catch (ex) {
            throw new InternalServerErrorException(`Error creating a log for indicator with id ${this.id}`);
        }
    }

    @AfterUpdate()
    async createLog() {
        const query = await IndicatorLog.find({
            where: {
                indicatorId: this.id,
            },
            order: {
                id: "DESC"
            },
            take: 1
        });
        const latest = query.pop();
        const log = new IndicatorLog();

        if (this.current_value === latest.current_value) {
            return;
        }
        log.current_value = this.current_value;
        log.goal = this.goal;
        log.indicatorId = this.id;
        log.units = this.units;

        try {
            await log.save();
        } catch (ex) {
            throw new InternalServerErrorException(`Error creating log for indicator with id ${this.id}`);
        }
    }
}