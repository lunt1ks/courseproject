import { BaseEntity, Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class IndicatorLog extends BaseEntity {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "timestamp without time zone",
        default: () => 'CURRENT_TIMESTAMP', nullable: true
    })
    date: Date;

    @Column()
    indicatorId: number;

    @Column({nullable:true})
    current_value: number;

    @Column({nullable:true})
    goal: number;

    @Column({nullable: true})
    units: string;
}