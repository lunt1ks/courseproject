import { Repository, EntityRepository } from "typeorm";
import { Indicator } from "./entities/indicator.entity";

@EntityRepository(Indicator)
export class IndicatorRepository extends Repository<Indicator> {

}