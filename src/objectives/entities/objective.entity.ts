import {BaseEntity, Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Kpi} from "../../kpi/entities/kpi.entity";
import {Company} from "../../companies/company.entity";
import {Initiative} from "../../initiatives/initiative.entity";
import {User} from "../../auth/entities/user.entity";
import {Statuses} from "../../common/enums/status.enum";
import {AfterInsert, AfterUpdate, BeforeUpdate} from "typeorm/index";
import {ObjectiveLog} from "./objective-log.entity";
import {InternalServerErrorException} from "@nestjs/common";


@Entity()
export class Objective extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true})
    title: string;

    @Column({type: "date", nullable: true})
    start_date;

    @Column({type: "date", nullable: true})
    end_date;

    @OneToMany(type => Kpi, kpi => kpi.objective, {cascade: true})
    kpi: Kpi[];

    @ManyToOne(type => Company, company => company.objectives,
        {onUpdate: "CASCADE", onDelete: "CASCADE"})
    company: Company;

    @Column({type: "text", default: Statuses.notStarted})
    status: string;

    @Column({type: "text", nullable: true})
    note: string;

    @ManyToMany(type => User, user => user.objectives, {cascade: false})
    @JoinTable()
    users: User[]

    @OneToMany(type => Initiative, initiative => initiative.objective, {cascade: true})
    initiatives: Initiative[]


}