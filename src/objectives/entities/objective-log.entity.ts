import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm/index";
import {Statuses} from "../../common/enums/status.enum";

@Entity()
export class ObjectiveLog extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({
        type: "timestamp without time zone",
        default: () => 'CURRENT_TIMESTAMP', nullable: true
    })
    date: Date

    @Column()
    objectiveId: number

    @Column({nullable: true})
    ownerId: number

    @Column({nullable: true})
    note: string

    @Column({default: Statuses.notStarted})
    status: string
}