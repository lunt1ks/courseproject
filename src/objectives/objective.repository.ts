import {EntityRepository, Repository} from "typeorm";
import {Objective} from "./entities/objective.entity";

@EntityRepository(Objective)
export class ObjectiveRepository extends Repository<Objective> {

}