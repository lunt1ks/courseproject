import {Module} from '@nestjs/common';
import {ObjectivesController} from './objectives.controller';
import {ObjectivesService} from './objectives.service';
import {ObjectiveRepository} from "./objective.repository";
import {TypeOrmModule} from "@nestjs/typeorm";
import {AuthModule} from "../auth/auth.module";

@Module({
    imports: [
        AuthModule,
        TypeOrmModule.forFeature([ObjectiveRepository])
    ],
    controllers: [ObjectivesController],
    providers: [ObjectivesService]
})
export class ObjectivesModule {
}
