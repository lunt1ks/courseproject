import {IsDate, IsIn, IsNotEmpty, IsOptional, IsString, MinLength} from "class-validator";
import {Statuses} from "../../common/enums/status.enum";

export class CreateObjectiveDto {

    @IsNotEmpty()
    @IsOptional()
    start_date

    @IsNotEmpty()
    @IsOptional()
    end_date

    @IsNotEmpty()
    @IsIn([
        Statuses.atRisk,
        Statuses.onTrack,
        Statuses.offTrack,
        Statuses.completed,
        Statuses.notStarted,
        Statuses.aheadOfSchedule
    ])
    status:string

    @IsString()
    @MinLength(4)
    @IsOptional()
    title: string;

    @IsString()
    @IsOptional()
    note: string;

}