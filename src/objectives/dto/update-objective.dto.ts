import {IsDate, IsIn, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from "class-validator";
import {Statuses} from "../../common/enums/status.enum";

export class UpdateObjectiveDto {

    @IsNotEmpty()
    @IsOptional()
    start_date

    @IsNotEmpty()
    @IsOptional()
    end_date

    @IsString()
    @MinLength(4)
    @IsOptional()
    name: string;

    @IsString()
    @IsOptional()
    note: string;

    @IsNotEmpty()
    @IsOptional()
    @IsIn([
        Statuses.atRisk,
        Statuses.onTrack,
        Statuses.offTrack,
        Statuses.completed,
        Statuses.notStarted,
        Statuses.aheadOfSchedule
    ])
    status:string


}