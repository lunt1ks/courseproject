import {
    Injectable,
    NotFoundException,
    InternalServerErrorException,
    ConflictException,
    ForbiddenException,
    BadRequestException
} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {ObjectiveRepository} from "./objective.repository";
import {UpdateObjectiveDto} from "./dto/update-objective.dto";
import {CreateObjectiveDto} from "./dto/create-objective.dto";
import {Objective} from "./entities/objective.entity";
import {Company} from "../companies/company.entity";
import {User} from "../auth/entities/user.entity";
import {Statuses} from "../common/enums/status.enum";
import {ObjectiveLog} from "./entities/objective-log.entity";
import {KpiLog} from "../kpi/entities/kpi-log.entity";
import {assign} from "nodemailer/lib/shared";

@Injectable()
export class ObjectivesService {
    constructor(
        @InjectRepository(ObjectiveRepository)
        private objectivesRepository: ObjectiveRepository
    ) {
    }

    async createObjective(companyId: number, createObjectiveDto: CreateObjectiveDto, user: User) {
        const company = await Company.findOne(companyId, {relations: ["objectives"]});

        if (!company) {

            throw new NotFoundException("Company with such id was not found");
        }
        if (!await this.permissions(company.ownerId, user)) {
            throw new ForbiddenException("You dont have permissions create objective");
        }


        try {
            const objective = new Objective();
            Object.assign(objective, createObjectiveDto);
            objective.initiatives = [];
            objective.kpi = [];
            objective.users = [];
            objective.users.push(user);
            await objective.save();
            const log: ObjectiveLog = new ObjectiveLog();
            log.status = createObjectiveDto.status;
            log.objectiveId = objective.id;
            log.ownerId = user.id;
            log.note = createObjectiveDto.note;
            await log.save()
            company.objectives.push(objective);
            await company.save();
            return objective;
        } catch (e) {
            throw new InternalServerErrorException('error:' + e.message);
        }

    }

    async getObjectivesForCompany(companyId: number): Promise<Objective[]> {

        return await this.objectivesRepository.createQueryBuilder('objective')
            .leftJoinAndSelect('objective.initiatives', 'initiative')
            .leftJoinAndSelect('objective.kpi', 'kpi')
            .leftJoinAndSelect('objective.company', 'company')
            .where('company.id = :companyId', {companyId})
            .getMany();

    }

    async getAllObjectives(): Promise<Objective[]> {

        return await this.objectivesRepository.find({relations: ["kpi", "initiatives"]});

    }


    async getObjectiveById(id: number, user: User) {
        const objective = await this.objectivesRepository.findOne(id, {
            relations: ["kpi", "initiatives", "initiatives.actions", "initiatives.actions.user", "users"]
        });
        if (!objective.users.some(item => item.id === user.id)) {
            if(user.role !== 'superAdmin') {
                throw new ForbiddenException("You dont have permissions");
            }
        }
        
        const objCompany = await this.objectivesRepository.findOne(id, {relations: ["company", "company.users"]})

        objective.kpi.sort(function (a, b) {
            return b.id - a.id;
        });

        objective.initiatives.sort(function (a, b) {
            return b.id - a.id;
        })

        for (const initiative of objective.initiatives) {
            initiative.actions.sort(function (a, b) {
                return b.id - a.id;
            });
        }

        const objLogs = await ObjectiveLog.find({where: {objectiveId: id}, order: {id: "ASC"}})
        for (const kpi of objective.kpi) {
            const logs = await KpiLog.find({where: {kpiId: kpi.id}});
            const log = {
                logs
            };
            Object.assign(kpi, log);
        }
        Object.assign(objective, {lastUpdate: objLogs[objLogs.length - 1].ownerId})
        Object.assign(objective, {objLogs})
        return {
            objective,
            company: {
                companyId: objCompany.company.id,
                companyName: objCompany.company.name,
                companyUsers: objCompany.company.users
            }
        }
    }

    async updateObjective(id: number, updateObjectiveDto: UpdateObjectiveDto, user: User): Promise<Objective> {
        const objective = await this.objectivesRepository.findOne(id, {relations: ['company']});
        if (!objective) throw new NotFoundException('No objective with id' + id)

        if (!await this.permissions(objective.company.ownerId, user)) {
            throw new ForbiddenException("You dont have permissions create objective");
        }
        const log = new ObjectiveLog();
        log.ownerId = user.id;
        log.objectiveId = objective.id;
        log.note = (updateObjectiveDto.note !== objective.note && updateObjectiveDto.note) ? updateObjectiveDto.note : null;
        log.status = updateObjectiveDto.status !== objective.status ? updateObjectiveDto.status : objective.status;
        let onSave = false
        if ((updateObjectiveDto.status !== objective.status && updateObjectiveDto.status) ||
            (updateObjectiveDto.note !== objective.note && updateObjectiveDto.note)) {
            onSave = true;
        }
        Object.assign(objective, updateObjectiveDto);

        try {
            await objective.save();
            if (onSave) await log.save()

        } catch (e) {
            throw new InternalServerErrorException("Cannot save: " + e.message);

        }
        return objective
    }

    async addUserToObjective(objectiveId: number, userId: number, owner: User) {
        const user = await User.findOne(userId);
        if (!user) throw new NotFoundException("User with id " + userId + " was not found");
        const company = await Company.findOne({ownerId: owner.id},
            {
                relations: [
                    'objectives',
                    'objectives.users',
                    'objectives.kpi'
                ]
            });
        if (!company) throw new NotFoundException("You don't have company yet");
        console.log(1)
        if (!await this.permissions(company.ownerId, owner)) {
            throw new ForbiddenException("You dont have permissions");
        }
        const objective = company.objectives.find(objective => objective.id === objectiveId)
        const exists = objective.users.some(user => user.id === userId);
        if (exists) throw new ConflictException('User is already exists in objective');
        objective.users.push(user);

        if (!objective) {
            throw new NotFoundException(`No objective with id ${objectiveId} was found`);
        }

        try{


            return await objective.save();;
        }catch (e){
            throw new InternalServerErrorException(e.message);

        }


    }

    async removeUserFromObjective(objectiveId: number, userId: number, owner: User) {
        const user = await User.findOne(userId);
        if (!user) throw new NotFoundException("User with id " + userId + " was not found");
        const company = await Company.findOne({ownerId: owner.id},
            {
                relations: [
                    'objectives',
                    'objectives.users',
                    'objectives.kpi'
                ]
            });
        if (!company) throw new NotFoundException("You don't have company yet");
        if (!await this.permissions(company.ownerId, owner)) {
            throw new ForbiddenException("You dont have permissions");
        }
        const objective = await this.objectivesRepository.findOne(objectiveId, {relations: ["users"]});
        const exists = objective.users.some(user => user.id === userId);
        if (!exists) throw new NotFoundException('User is not exists in objective');
        objective.users = objective.users.filter(item => item.id !== userId);
        try {
            await objective.save();
            await user.save();
        } catch (e) {
            throw new InternalServerErrorException(e.message);
        }
        return objective

    }

    async deleteObjective(id: number, user: User): Promise<void> {
        const objective = await this.objectivesRepository.findOne(id, {relations: ['company']});
        if (!objective) throw new NotFoundException('No objective with id' + id)
        if (user.role !== 'superAdmin') {
            if (objective.company.ownerId !== user.id) {
                throw new ForbiddenException("You dont have permissions delete objective")
            }
        }
        try {
            await this.objectivesRepository.delete({id});
        } catch (ex) {
            throw new InternalServerErrorException(`Error deleting objective: ${ex.message}`);
        }
    }

    private permissions = async (ownerId: number, user: User) => {
        if (user.role !== 'superAdmin') {
            if (ownerId !== user.id) {
                if (user.role !== 'collaborator') {
                    return false
                }
            }
        }
        return true
    }
}