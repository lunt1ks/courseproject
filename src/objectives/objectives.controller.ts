import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    UseGuards,
    ValidationPipe
} from '@nestjs/common';
import {ObjectivesService} from "./objectives.service";
import {UpdateObjectiveDto} from "./dto/update-objective.dto";
import {CreateObjectiveDto} from "./dto/create-objective.dto";
import {AuthGuard} from "@nestjs/passport";
import {Objective} from "./entities/objective.entity";
import {GetUser} from "../auth/custom-decorators/get-user.decorator";
import {User} from "../auth/entities/user.entity";
import {Roles} from "../auth/custom-decorators/roles.decorator";
import {RolesGuard} from 'src/auth/custom-guards/roles.guard';
import {ObjectiveLog} from "./entities/objective-log.entity";

@Controller('/api/objectives')
export class ObjectivesController {
    constructor(
        private objectivesService: ObjectivesService
    ) {
    }

    @Roles('superAdmin', 'admin', 'collaborator')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Post('/:companyId')
    async createObjective(@Body(ValidationPipe) createObjectiveDto: CreateObjectiveDto,
                          @Param('companyId', ParseIntPipe)companyId: number,
                          @GetUser() user: User): Promise<Objective> {
        return await this.objectivesService.createObjective(companyId, createObjectiveDto, user)
    }

    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Get()
    async getAllObjectives(): Promise<Objective[]> {
        return await this.objectivesService.getAllObjectives();
    }

    @Roles('superAdmin', 'admin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Post('add/user/:objectiveId')
    async addUserToObjective(@Param('objectiveId', ParseIntPipe) objectiveId: number,
                             @Body('userId', ParseIntPipe) userId: number,
                             @GetUser() owner: User) {
        return await this.objectivesService.addUserToObjective(objectiveId, userId, owner)
    }


    @Roles('superAdmin', 'admin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Delete('delete/user/:objectiveId')
    async deleteUserFromObjective(@Param('objectiveId', ParseIntPipe) objectiveId: number,
                                  @Body('userId', ParseIntPipe) userId: number,
                                  @GetUser() user: User) {
        return await this.objectivesService.removeUserFromObjective(objectiveId, userId, user)
    }

    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Get('company/:companyId')
    async getObjectivesForCompany(
        @Param("companyId", ParseIntPipe)companyId: number
    ): Promise<Objective[]> {
        return await this.objectivesService.getObjectivesForCompany(companyId);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('/:id')
    async getObjectiveById(
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User
        ) {
    return await this.objectivesService.getObjectiveById(id, user);
    }

    @Roles('superAdmin', 'admin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Patch('/:id')
    async updateObjective(@Param('id', ParseIntPipe) id: number,
                          @Body(ValidationPipe) updateObjectiveDto: UpdateObjectiveDto,
                          @GetUser() user: User): Promise<Objective> {
        return await this.objectivesService.updateObjective(id, updateObjectiveDto, user)
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    async deleteObjective(@Param('id', ParseIntPipe) id: number,
                          @GetUser() user: User): Promise<void> {
        return await this.objectivesService.deleteObjective(id, user)
    }
}
