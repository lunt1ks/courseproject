import {
    Injectable,
    NotFoundException,
    UnauthorizedException,
    InternalServerErrorException,
    ForbiddenException,
    BadRequestException,
    ConflictException
} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {JwtService} from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { User } from './entities/user.entity';
import { UserRepository } from './user.repository';
import { JwtPayload } from '../common/interfaces/jwt-payload.interface';
import { LoginResult } from '../common/interfaces/login-result.interface';
import { LoginCredentialsDto } from './dto/login-credentials.dto';
import { SignUpCredentialsDto } from './dto/signup-credentials.dto';
import { EditUserDto } from './dto/edit-user.dto';
import { UpdatePasswordDto } from './dto/update-password.dto';
import { MailSenderService } from 'src/mailer/mail-sender.service';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { ConfigService } from 'src/config/config.service';
import { Invite } from 'src/invite/invite.entity';
import { Company } from 'src/companies/company.entity';

const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'; //get rid of ugly global variable here

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(UserRepository)
        private readonly userRepository: UserRepository,
        private jwtService: JwtService,
        private mailService: MailSenderService,
        private configService: ConfigService
    ) {
    }

    async googleLogin(user: User, response: any): Promise<void> {

        if (!user) {
            throw new BadRequestException('No user in request object')
        }

        const found = await this.userRepository.findOne({email: user.email}, {select: ['id', 'email', 'role']});

        if (!found) {
            try {
                const newUser = new User();
                Object.assign(newUser, user);
                newUser.fromGoogle = true;
                const invite = await Invite.findOne({email: user.email});
                
                if(invite) {
                const company = await Company.findOne(invite.companyId, {relations:['users']});
                 
                        newUser.role = invite.role;
                        await newUser.save()
                        company.users.push(newUser);
                        await company.save();
                        await invite.remove();
                }

                await newUser.save();
                const {id, role, email} = newUser;
                const payload = {id, role, email};

                const {accessToken, refreshToken} = await this.generateTokens(newUser, payload);

                const data = {subject: 'Welcome to okapi!', text: `Hello, ${newUser.name}\n Welcome to okapi`}
                this.mailService.sendCustomEmail(newUser.email, data);
                return response.redirect(`${this.configService.get('FRONT_REDIRECT_GOOGLE_AUTH_NEWUSER_URL')}?accessToken=${accessToken}&refreshToken=${refreshToken}`); 
            }
            catch(ex) {
                throw new InternalServerErrorException(`${ex.message}`);
            }
        }
        
        if(!await User.findOne(found.id, {select: ['password']})) {
            found.fromGoogle = true;
            await found.save();
        }
        const { id, email, role } = found;
        const payload = { id, email, role };
        const { accessToken, refreshToken } = await this.generateTokens(found, payload);
        return response.redirect(`${this.configService.get('FRONT_REDIRECT_GOOGLE_AUTH_URL')}?accessToken=${accessToken}&refreshToken=${refreshToken}`);
    }

    async signUp(signUpCredentialsDto: SignUpCredentialsDto): Promise<LoginResult> {
        const login = new LoginCredentialsDto;
        login.email = signUpCredentialsDto.email;
        login.password = signUpCredentialsDto.password;
        signUpCredentialsDto.password = await this.hashPassword(signUpCredentialsDto.password);
        await this.userRepository.createUser(signUpCredentialsDto);
        const data = {subject: 'Welcome to okapi!', text: `Hello,  ${signUpCredentialsDto.name}\n Welcome to okapi`}
        this.mailService.sendCustomEmail(signUpCredentialsDto.email, data);
        return await this.login(login);
    }

    async login({email, password}: LoginCredentialsDto): Promise<LoginResult> {
        const found = await this.userRepository.findOne({email}, {select: ['password']});

        if (!found) {
            throw new NotFoundException(`User with email ${email} is not registered or did not set the password`);
        }

        const flag = await found.validatePassword(password);

        if (!flag) {
            throw new UnauthorizedException('Invalid password');
        } else {
            const user = await this.userRepository.findOne({email}, {select: ['id', 'email', 'role']});
            const {id, role} = user;
            const payload = {id, role, email};
            const {accessToken, refreshToken} = await this.generateTokens(user, payload);

            return {id, email, accessToken, refreshToken}
        }
    }

    async getAll(): Promise<User[]> {
        return await this.userRepository.find();
    }

    async getUserByEmail(email: string): Promise<User> {
        const found = await this.userRepository.findOne({email});

        if (!found) {
            throw new NotFoundException(`User with email ${email} was not found`);
        }

        return found;
    }

    async getUserById(id: number): Promise<User> {
        const found = await this.userRepository.findOne(id);

        if (!found) {
            throw new NotFoundException(`No user with id ${id} was found`);
        }

        return found;
    }

    async updateMe({id}: User, data: EditUserDto): Promise<User> {
        if (!EditUserDto) {
            return null;
        }
        const user = await this.userRepository.findOne(id);

        if (!user) {
            throw new NotFoundException(`No user with id ${id} registered`);
        }


        if (!data.name) {
            data.name = user.name;
        }

        if (!data.avatar) {
            data.avatar = user.avatarPath;
        }

        Object.assign(user, data);

        try {
            return await user.save();
        } catch (ex) {
            throw new InternalServerErrorException(`Error updating user info: ${ex.message}`);
        }
    }

    async changeRole(user: User, role: string, userId: number, companyId: number) {
        const company = await Company.findOne(companyId, {relations:['users']});
        if (user.role !== 'superAdmin') {
            if (user.id !== company.ownerId) {
                throw new ForbiddenException("You dont have permissions to change user roles");
            }
        }
        if(userId === company.ownerId) throw new ForbiddenException("You can't change your role");
        if (!company) throw new NotFoundException("No company with id " + companyId);
        if(!company.users.some(item=>item.id===userId)) {
            throw new NotFoundException("No such user in company, please send invite via email")
        }
        const userToChange = await User.findOne(userId);
        if (!userToChange) throw new NotFoundException("No user with id " + companyId)
        userToChange.role = role;
        return userToChange.save();

    }

    async requestResetCode(email: string): Promise<string> {
        const foundUser = await this.userRepository.findOne({email});
        if (!foundUser) {
            throw new NotFoundException(`User with email "${email}"is not registered!`);
        }

        try {
            const code = this.generateCode(alphabet, 4);
            const {email} = foundUser;
            foundUser.reset_pwd_code = code;
            await foundUser.save();
            setTimeout(async () => {
                const user = await this.userRepository.findOne({email});
                user.reset_pwd_code = null;
                await user.save();
            }, 600000);
            return await this.mailService.sendResetCode(email, code);
        } catch (ex) {
            throw new InternalServerErrorException(`Error: ${ex.message}`);
        }
    }

    async resetPassword({email, code, newPassword}: ResetPasswordDto): Promise<void> {
        const user = await this.userRepository.findOne({email});

        if (!user) {
            throw new NotFoundException(`User with email ${email} was not found`);
        }

        if (!user.reset_pwd_code) {
            throw new NotFoundException(`Reset code of user with email ${email} has expired or never been requested`)
        }

        if (code != user.reset_pwd_code) {
            throw new ForbiddenException('Invalid code!');
        } else {
            user.password = await this.hashPassword(newPassword);
            user.reset_pwd_code = null;
            user.fromGoogle = false;
        
            try {
                user.save();
            } catch (ex) {
                throw new InternalServerErrorException(`${ex.message}`);
            }
        }

    }

    async updatePassword({id}: User, {oldPassword, newPassword}: UpdatePasswordDto) {
        const found = await this.userRepository.findOne(id, {select: ['password']});

        if (!found) {
            const found = await this.userRepository.findOne(id);

            if (!found) {
                throw new NotFoundException(`User with id ${id} was not found`);
            } else {
                found.password = await this.hashPassword(newPassword);
                found.fromGoogle = false;
                await found.save();
                return;
            }
        }

        const flag = await found.validatePassword(oldPassword);

        if (flag) {
            const user = await this.userRepository.findOne(id);
            user.password = await this.hashPassword(newPassword);
            user.fromGoogle = false;
            try {
                await user.save();
                return;
            } catch(ex) {
                throw new InternalServerErrorException(`Error changing password: ${ex.message}`);
            }

        } else {
            throw new ForbiddenException('Invalid password');
        }
    }

    async deleteUserById(id: number, companyId: number): Promise<void> {
        const found = await this.userRepository.findOne(id, {relations: ['actions']});

        if (!found) {
            throw new NotFoundException(`No user with id ${id} was found`);
        }

        const company = await Company.findOne(companyId, {relations: ['users']});
        const owner = await User.findOne(company.ownerId);

        if(found.actions.length) {
            for  (const action of found.actions) {
                action.ownerId = company.ownerId;
                action.user = owner;
                await action.save();
            }
        }
        try {
            await found.remove();
        } catch (ex) {
            throw new InternalServerErrorException(`Error deleting user: ${ex.message}`);
        }
    }

    async deleteMe({id}: User, companyId: number): Promise<void> {
        await this.deleteUserById(id, companyId);
    }

    private async generateTokens(user: User, payload: JwtPayload): Promise<{ accessToken: string, refreshToken: string }> {
        const accessToken = await this.jwtService.sign(payload);
        const refreshToken = await this.jwtService.sign({email: user.email}, {expiresIn: '180d'});
        /*eslint-disable-next-line*/
        user.refresh_token = refreshToken;

        try {
            await user.save();
        } catch (ex) {
            throw new InternalServerErrorException(`An error occured while assigning tokens: ${ex.message}`);
        }

        return Object.assign({},
            {accessToken, refreshToken});

    }


    async hashPassword(password: string): Promise<string> {
        const salt = await bcrypt.genSalt(10);
        return await bcrypt.hash(password, salt);
    }

    private generateCode(alphabet: string, length: number): string {
        let result = '';
        const alphabetLength = alphabet.length;
        for (let i = 0; i < length; i++) {
            result += alphabet.charAt(Math.floor(Math.random() * alphabetLength));
        }
        return result;
    }

}
