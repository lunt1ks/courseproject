import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { ConfigModule } from 'src/config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from 'src/config/config.service';
import { JwtStrategy } from './jwt.strategy';
import { GoogleStrategy } from './google.strategy';
import { MailSenderService } from 'src/mailer/mail-sender.service';
import { MailerModule } from '@nest-modules/mailer'
import { mailerConfig } from 'src/config/mailer.config';

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forFeature([UserRepository]),
    PassportModule.register({defaultStrategy: 'jwt'}),
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return mailerConfig(configService);
      },
      inject: [ConfigService]
    }),
    JwtModule.registerAsync({
      imports: [
        ConfigModule
      ],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
        signOptions: {
          expiresIn: configService.get('JWT_ACCESS_TOKEN_EXPIRES')
        }
      }),
      inject: [ ConfigService ],
    })
  ],

  controllers: [
      AuthController
    ],

  providers: [
      AuthService,
      JwtStrategy,
      GoogleStrategy,
      MailSenderService
    ],

  exports: [
    JwtStrategy,
    PassportModule,
    GoogleStrategy,
    MailSenderService
  ]
})
export class AuthModule {}
