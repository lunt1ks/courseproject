import { Injectable, UnauthorizedException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import {PassportStrategy} from '@nestjs/passport';
import {Strategy, ExtractJwt } from 'passport-jwt';
import { ConfigService } from "src/config/config.service";
import { UserRepository } from "./user.repository";
import { JwtPayload } from "../common/interfaces/jwt-payload.interface";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        @InjectRepository(UserRepository)
        private readonly userRepository: UserRepository,
        private readonly configService: ConfigService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.get('JWT_SECRET'),
            signOptions: {
                expiresIn: configService.get('JWT_ACCES_TOKEN_EXPIRES')
            }
        })
    }

    async validate(payload: JwtPayload) {
        const { id, email } = payload;
        let user = await this.userRepository.findOne(id, {select: ['id', 'email', 'role', 'fromGoogle', 'avatarPath']});
        
        if(!user) {
            //in case user`s regenerating his tokens using refreshToken 
            user = await this.userRepository.findOne({email}, {select: ['id', 'email', 'role', 'fromGoogle', 'avatarPath']})
                
                if(!user) {
                    throw new UnauthorizedException();
                }
        }
        return user;
    }
}