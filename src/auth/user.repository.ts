import { EntityRepository, Repository } from "typeorm";
import { User } from "./entities/user.entity";
import { SignUpCredentialsDto } from "./dto/signup-credentials.dto";
import { InternalServerErrorException, ConflictException } from "@nestjs/common";
import { Role } from "../common/enums/user-roles.enum";
import {Invite} from "../invite/invite.entity";
import {Company} from "../companies/company.entity";


const uniqueConstraintCode = 23505;

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    async createUser(signUpCredentialsDto: SignUpCredentialsDto): Promise<User> {
            const user = new User();
            Object.assign(user, signUpCredentialsDto);
            const invite = await Invite.findOne({email:signUpCredentialsDto.email});

            if(invite) {
                const company = await Company.findOne(invite.companyId, {relations:['users']});
                await user.save();
                company.users.push(user);
                user.role = invite.role;
                await company.save();
                await invite.remove()
            }
            try {
               return await user.save();
            }
            catch(ex) {
                    if(ex.code == uniqueConstraintCode) {
                        throw new ConflictException(`User with such email is already registered!`);
                    }

            throw new InternalServerErrorException(`Failed to create a user: ${ ex.message }`);
        }
    }
}
