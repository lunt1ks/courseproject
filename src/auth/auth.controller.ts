import {
    Controller,
    Get,
    Post,
    ValidationPipe,
    Body,
    UseGuards,
    Param,
    ParseIntPipe,
    Delete,
    Patch,
    Req,
    Redirect,
    Res,
    Query
} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {User} from './entities/user.entity';
import {AuthService} from './auth.service';
import {LoginResult} from '../common/interfaces/login-result.interface';
import {GetUser} from './custom-decorators/get-user.decorator';
import {SignUpCredentialsDto} from './dto/signup-credentials.dto';
import {LoginCredentialsDto} from './dto/login-credentials.dto';
import {GetUserByEmailDto} from './dto/get-user-by-email.dto';
import {EditUserDto} from './dto/edit-user.dto';
import {UpdatePasswordDto} from './dto/update-password.dto';
import {RolesGuard} from './custom-guards/roles.guard';
import {Roles} from './custom-decorators/roles.decorator';
import {ResetPasswordDto} from './dto/reset-password.dto';
import {response} from 'express';
import {ConfigService} from 'src/config/config.service';
import {ChangeRoleDto} from "./dto/change-role.dto";

@Controller('api/auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly configService: ConfigService
    ) {
    }

    // @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Get('/get/all')
    async GetUsers(): Promise<User[]> {
        return this.authService.getAll();
    }

    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'))
    @Get('/get/:id')
    async GetUserById(
        @Param('id', ParseIntPipe) id: number,
    ): Promise<User> {
        return this.authService.getUserById(id);
    }

    @Get('/google')
    @UseGuards(AuthGuard('google'))
    googleAuth(
        @Res() response,
    ) {
        const redirectUrl = `https://accounts.google.com/signin/oauth/oauthchooseaccount?response_type=code&redirect_uri=${this.configService.get('FRONT_REDIRECT_GOOGLE_AUTH_URL')}&scope=email profile&client_id=${this.configService.get('GOOGLE_CLIENT_ID')}&o2v=2&as=cRI31Hl1byU0ow5ZCYkplA&flowName=GeneralOAuthFlow`
        response.redirect(`${redirectUrl}`);
    }

    @Get('/google/redirect')
    @UseGuards(AuthGuard('google'))
    googleAuthRedirect(
        @GetUser() user: User,
        @Res() response
    ) {
        return this.authService.googleLogin(user, response);
    }

    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'))
    @Get('/get')
    async GetUserByEmail(
        @Body(ValidationPipe) {email}: GetUserByEmailDto
    ): Promise<User> {
        return this.authService.getUserByEmail(email);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('/me')
    async GetMe(
        @GetUser() {id}: User,
    ): Promise<User> {
        return this.authService.getUserById(id);
    }

    @Post('/signup')
    async SignUp(
        @Body(ValidationPipe) createUserDto: SignUpCredentialsDto
    ): Promise<LoginResult> {
        return this.authService.signUp(createUserDto);
    }

    @Post('/login')
    async Login(
        @Body(ValidationPipe) loginCredentialsDto: LoginCredentialsDto,
    ): Promise<LoginResult> {
        return this.authService.login(loginCredentialsDto);
    }

    @UseGuards(AuthGuard('jwt'))
    @Patch('/me/edit')
    async EditInfo(
        @GetUser() user: User,
        @Body(ValidationPipe) editUserDto: EditUserDto,
    ): Promise<User> {
        return this.authService.updateMe(user, editUserDto);
    }

    @Roles('admin', 'superAdmin')
    @UseGuards(AuthGuard('jwt'))
    @Patch('/change/role/:companyId/:userId')
    async changeRole(
        @Param('userId', ParseIntPipe) userId: number,
        @Param('companyId', ParseIntPipe) companyId: number,
        @GetUser() user: User,
        @Body(ValidationPipe) changeRoleDto: ChangeRoleDto,
    ): Promise<User> {
        return this.authService.changeRole(user, changeRoleDto.role, userId, companyId);
    }

    @UseGuards(AuthGuard('jwt'))
    @Patch('/me/password')
    async UpdatePassword(
        @GetUser() user: User,
        @Body(ValidationPipe) updatePasswordDto: UpdatePasswordDto
    ): Promise<void> {
        return this.authService.updatePassword(user, updatePasswordDto);
    }

    @Patch('/requestReset')
    async RequestPasswordResetCode(
        @Body('email') email: string,
    ): Promise<string> {
        return this.authService.requestResetCode(email);
    }

    @Patch('reset')
    async ResetPassword(
        @Body(ValidationPipe) resetPasswordDto: ResetPasswordDto
    ): Promise<void> {
        return this.authService.resetPassword(resetPasswordDto);
    }

    @Roles('superAdmin')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Delete('/:companyId/:id')
    async DeleteUser(
        @Param('companyId', ParseIntPipe) companyId: number,
        @Param('id', ParseIntPipe) id: number,
    ): Promise<void> {
        return this.authService.deleteUserById(id, companyId);
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete('/:companyId/me')
    async DeleleMe(
        @Param('companyId', ParseIntPipe) companyId: number,
        @GetUser() user: User,
    ): Promise<void> {
        return this.authService.deleteMe(user, companyId);
    }
}
