import {
    Entity,
    Unique,
    BaseEntity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    ManyToMany,
    JoinTable,
    OneToMany
} from "typeorm";
import * as bcrypt from 'bcryptjs';
import { Company } from "src/companies/company.entity";
import {Action} from "../../actions/action.entity";
import {Objective} from "../../objectives/entities/objective.entity";
import { ConflictException } from "@nestjs/common";


@Entity()
@Unique(['email'])
export class User extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column('text', {nullable: true, default: 'admin'})
    role: string;

    @Column({select: false, nullable: true})
    password: string;

    @Column({ nullable: true })
    avatarPath: string;

    @Column({nullable: true, default: false})
    fromGoogle: boolean;

    @Column({nullable: true, select: false})
    refresh_token: string;

    @Column({nullable: true})
    reset_pwd_code: string;

    @ManyToMany(type => Company, company => company.users, {cascade: false})
    companies: Company[];

    @ManyToMany(type=>Objective, objective=>objective.users, {cascade: false})
    objectives:Objective[]

    @OneToMany(type=>Action, action=>action.user, { cascade: false})
    actions:Action[];

    async validatePassword(password: string): Promise<boolean> {
        if(this.password == null) {
            throw new ConflictException('Password is not set');
        }
        return await bcrypt.compare(password, this.password);
    }
}