import { IsString, IsNotEmpty, IsEmail, IsIn, IsOptional, MinLength, Matches } from 'class-validator';

export class SignUpCredentialsDto {
    
    @IsString()
    @IsNotEmpty()
    name: string;

    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(8)
    @Matches(
        /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, 
        { message: 'Password should consist of at least 1 uppercase letter, 1 lowercase letter and at least 1 number or special character'}
    )
    password: string;

    @IsOptional()
    @IsString()
    @IsIn(['contributor', 'admin', 'superAdmin'])
    role: string;

    @IsOptional()
    @IsString()
    avatarPath: string;
}