import {IsString, IsNotEmpty, IsIn} from "class-validator";

export class ChangeRoleDto {

    @IsIn(['collaborator', 'contributor'])
    @IsString()
    @IsNotEmpty()
    role: string;
}
