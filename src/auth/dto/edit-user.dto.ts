import { IsOptional, IsString, IsNotEmpty, MinLength } from "class-validator";

export class EditUserDto {

    @IsOptional()
    @IsString()
    @IsNotEmpty()
    name: string;

    @IsOptional()
    @IsString()
    @IsNotEmpty()
    avatar: string;
}
