import { IsString, IsNotEmpty, IsEmail, MinLength, Matches } from "class-validator";

export class ResetPasswordDto {
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(4)
    code: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(8)
    @Matches(
        /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, 
        { message: 'Password should consist of at least 1 uppercase letter, 1 lowercase letter and at least 1 number or special character'
    })
    newPassword: string;
}