import { IsEmail, IsString, IsNotEmpty } from "class-validator";

export class GetUserByEmailDto {

    @IsString()
    @IsNotEmpty()
    @IsEmail()
    email: string;
}