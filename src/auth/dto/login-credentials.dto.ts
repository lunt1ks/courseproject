import { IsEmail, IsNotEmpty, IsString, MinLength, Matches } from "class-validator";

export class LoginCredentialsDto {

    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(8)
    @Matches(
        /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, 
        { message: 'Password should consist of at least 1 uppercase letter, 1 lowercase letter and at least 1 number or special character'
    })
    password: string;
}