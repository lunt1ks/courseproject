import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy, VerifyCallback} from 'passport-google-oauth20';
import { ConfigService } from "src/config/config.service";
import { User } from "./entities/user.entity";

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {

    constructor(
        private readonly configService: ConfigService
    ) {
        super( {
            clientID: configService.get('GOOGLE_CLIENT_ID'),
            clientSecret: configService.get('GOOGLE_SECRET'),
            callbackURL: configService.get('GOOGLE_REDIRECT_URL'),
            scope: ['email', 'profile']
        });
    }

    async validate(accessToken: string, refreshToken: string, profile: any, done: VerifyCallback): Promise<any> {

        const { name, emails, photos } = profile;

        const found = await User.findOne({email: emails[0].value}, {select: ['id', 'email', 'role']});

        if(!found) {
        const user = {
            email: emails[0].value,
            name: name.givenName + ' ' + name.familyName,
            avatarPath: photos[0].value || null,
            role: 'admin',
        }

        done(null, user);
    }
        else {
            done(null, found);
        }
    }
}