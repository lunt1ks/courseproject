import {Injectable} from "@nestjs/common";
import {MailerService} from '@nest-modules/mailer';
import {Cron} from '@nestjs/schedule';
import {Company} from "../companies/company.entity";
import {User} from "../auth/entities/user.entity";


@Injectable()
export class MailSenderService {
    constructor(
        private readonly mailerService: MailerService,
    ) {
    }

    public async sendCustomEmail(to: string, data: any): Promise<any> {
        try {
            await this.mailerService
        .sendMail({
            to,
            subject: data.subject,
            text: data.text
        });
        return 'Message sent!' 
        } catch(ex) {
            console.log(`Error sending mail: ${ex.message}`);
        }
    }

    public async sendResetCode(to: string, code: string): Promise<any> {
        try {
            await this.mailerService
            .sendMail({
                to,
                subject: 'Reset password code',
                text: `${code}`,
            });
        return 'Message sent!' 
        } catch(ex) {
            console.log(`Error sending mail: ${ex.message}`);
        }
    }

    @Cron('0 9 * * fri')
    async handleCron() {
        const updateKpiCompanies = await Company.find({where: {updateKpi: true}, select: ['id', 'ownerId']});
        const updateActionsCompanies = await Company.find({where: {updateAction: true}, select: ['id', 'ownerId']})
        
        const kpiEmails = [];
        const actionsEmails = [];

        updateKpiCompanies.map(async (item) => {
            const user = await User.findOne(item.ownerId)
            kpiEmails.push(user.email);
        });

        updateActionsCompanies.map(async (item) => {
            const user = await User.findOne(item.ownerId);
            actionsEmails.push(user.email);
        });
        const sendKpi = kpiEmails.join(', ')
        const sendActions = actionsEmails.join(', ')

        if (sendKpi) {
            await this.mailerService
                .sendMail({
                    to: sendKpi,
                    subject: 'Please update your KPI',
                    text: `Don\'t forget to update your KPI`,
                });
        }
        if (sendActions) {
            await this.mailerService
                .sendMail({
                    to: sendActions,
                    subject: 'Please update your ACTIONS',
                    text: `Don\'t forget to update your ACTIONS`,
                });
        }

    }

}
